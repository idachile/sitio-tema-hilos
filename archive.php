<?php
/*Template Name: Archivo*/
get_header();
?>
	<main>
		<section class="horizon bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">			
					<div class="gridle-gr-3 gridle-gr-12@medium"><?php echo getFiltroProducto();?></div>
					<div class="gridle-gr-9 gridle-gr-12@medium gridle-no-gutter">
						<div class="gridle-row">
						<!-- productos -->	
						<?php $args = array('post_type' => 'producto','posts_per_page' => -1,);
							echo getListadoProductos($args);?>
					</div>	
				</div>
		</section>
	</main>

<?php get_footer(); ?>