<?php
/*Template Name:Portadilla*/
get_header();
?>
<main>
	<?php
        $args = array('post_type' => 'page','post_status' => 'publish','posts_per_page' => 3);
        echo getDapartamentos($args);
    ?>

	<section class="horizon horizon__inner bg-white">
		<?php
        $on_off = get_field('habilitar_productos_destacados');
        if ($on_off):
          $args = array('post_type'=>'producto','posts_per_page'=>4);
          $destacados_array = get_field('productos_destacados');

					if (!empty($destacados_array)):
						$args['post__in'] = $destacados_array;
					endif;

					if ($post->post_name=="vestimentas"):
						$args['tax_query'] =  array(
								array(
									'taxonomy' => 'categoria_producto',
									'field'    => 'slug',
									'terms'    => $post->post_name,
									));
					endif;

					echo getProductosDestacados($args,3);
				endif
		?>
	</section>
</main>

<?php get_footer(); ?>
