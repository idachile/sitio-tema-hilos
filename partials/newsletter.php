<div class="main_color_dark_bg">
    <h2>suscríbrete al newsletter</h2>
    <p>Suscríbete a nuestro newsletter y recibe semanalmente en tu email todas las novedades de Lorem Ipsum Dolor.</p>
    <form method="post" action="" class="form-control__group">
        <div class="form-control">
        <label class="form-control__label" for="nombre_newsletter">Nombre</label>
        <input type="text" class="form-control__field" name="nombre_newsletter" placeholder="Ingrese su nombre"/></div>
        <div class="form-control">
        <label class="form-control__label" for="correo_newsletter">Correo</label>
        <input type="text" class="form-control__field" name="correo_newsletter" placeholder="Ingrese su correo"/></div>
        
        <input class="button button--extralarge" type="submit" value="suscribirme ahora">
    </form>
</div>