<div class="slider" data-module="slider" style="visibility: visible; backface-visibility: hidden; perspective: 1000px;">
									<div class="slider__items" data-role="slider-list" style="width: 3642px;">
										<div class="slider__slide" data-role="slider-slide" data-index="0" data-offset="0" style="width: 1214px; float: left; transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">
											<article class="horizon common-box common-box--horizontal common-box--featured common-box--b-bottom no-box-shadow">
												<figure class="common-box__figure">
													<a href="#">
														<img src="images/img/img-news-7.jpg">
													</a>
												</figure>
												<div class="common-box__body common-box__body--no-border">
													<p class="common-box__meta">Epígrafe de la noticia o artículo</p>
													
													<h2 class="common-box__title">
														<a href="#">Lorem ipsum dolor sit amet consecteur adipiscing elit maecenas element</a>
													</h2>
													
													<p class="common-box__meta"><em>12 de febrero, por</em> <a href="#">Nombre del autor</a></p>
													<p class="common-box__meta"><a href="#">Categoría 1</a>, <a href="#">Categoría 2</a></p>

													<p class="common-box__excerpt">
														Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ele ...
													</p>


												</div>
											</article>
										</div>
										<div class="slider__slide" data-role="slider-slide" data-index="1" data-offset="-2428" style="width: 1214px; float: left; transition-duration: 0ms; transform: translate(-2428px, 0px) translateZ(0px);">
											<article class="horizon common-box common-box--horizontal common-box--featured common-box--b-bottom no-box-shadow">
												<figure class="common-box__figure">
													<a href="#">
														<img src="images/img/img-news-2.jpg">
													</a>
												</figure>
												<div class="common-box__body common-box__body--no-border">
													<p class="common-box__meta">Epígrafe de la noticia o artículo</p>
													
													<h2 class="common-box__title">
														<a href="#">Lorem ipsum dolor sit amet consecteur adipiscing elit maecenas element</a>
													</h2>
													
													<p class="common-box__meta"><em>12 de febrero, por</em> <a href="#">Nombre del autor</a></p>
													<p class="common-box__meta"><a href="#">Categoría 1</a>, <a href="#">Categoría 2</a></p>

													<p class="common-box__excerpt">
														Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ele ...
													</p>


												</div>
											</article>
										</div>
										<div class="slider__slide" data-role="slider-slide" data-index="2" data-offset="-2428" style="width: 1214px; float: left; transition-duration: 0ms; transform: translate(-2428px, 0px) translateZ(0px);">
											<article class="horizon common-box common-box--horizontal common-box--featured common-box--b-bottom no-box-shadow">
												<figure class="common-box__figure">
													<a href="#">
														<img src="images/img/img-news-3.jpg">
													</a>
												</figure>
												<div class="common-box__body common-box__body--no-border">
													<p class="common-box__meta">Epígrafe de la noticia o artículo</p>
													
													<h2 class="common-box__title">
														<a href="#">Lorem ipsum dolor sit amet consecteur adipiscing elit maecenas element</a>
													</h2>
													
													<p class="common-box__meta"><em>12 de febrero, por</em> <a href="#">Nombre del autor</a></p>
													<p class="common-box__meta"><a href="#">Categoría 1</a>, <a href="#">Categoría 2</a></p>

													<p class="common-box__excerpt">
														Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ele ...
													</p>


												</div>
											</article>
										</div>
										
									</div>

									<div class="slider__bullets font-centered">
										<button class="slider__bullet" data-role="slider-bullet" data-target="0"></button>
										<button class="slider__bullet" data-role="slider-bullet" data-target="1"></button>
										<button class="slider__bullet slider__bullet--current" data-role="slider-bullet" data-target="2"></button>
								
									</div>
								</div>