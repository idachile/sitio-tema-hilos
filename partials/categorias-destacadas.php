<div class="gridle-gr-12 gridle-gr-12@medium">
    <h2 class="main-title">
        <a href="#" title="titulo">Productos Destacados</a>
    </h2>
    <div class="gridle-row">
        <div class="gridle-gr-3 gridle-gr-12@medium">
            <a href="#" title="titulo">
                <img src="images/img/img-product-3.jpg" class="cover-img">
            </a>
            <div class="common-box__header">
                <div class="overlap--footer">Categoría 3 más extensa dos líneas</div>
            </div>
        </div>
        <div class="gridle-gr-3 gridle-gr-12@medium">
            <a href="#" title="titulo">
                <img src="images/img/img-product-6.jpg" class="cover-img">
            </a>
            <div class="common-box__header">
                <div class="overlap--footer">Categoría 4</div>
            </div>
        </div>
        <div class="gridle-gr-3 gridle-gr-12@medium">
            <a href="#" title="titulo">
                <img src="images/img/img-product-8.jpg" class="cover-img">
            </a>
            <div class="common-box__header">
                <div class="overlap--footer">Categoría 5</div>
            </div>
        </div>
        <div class="gridle-gr-3 gridle-gr-12@medium">
            <a href="#" title="titulo">
                <img src="images/img/img-product-9.jpg" class="cover-img">
            </a>
            <div class="common-box__header">
                <div class="overlap--footer">Categoría 6</div>
            </div>
        </div>
    </div>
</div>