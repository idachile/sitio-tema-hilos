<article>
    <div class="common-box__header">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3329.52113801051!2d-70.6052624488622!3d-33.435726780683865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf75cd8bfd95%3A0xf8594e5a1921d2dd!2sSuecia+1709%2C+Providencia%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1472497215311" width="400" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="common-box__body">
        <p class="common-box__excerpt">
            <i class="icon-elem icon-elem--place font-color-grey-dark" ></i>
            <a href="#" title="titulo" class="font-color-grey-darkest"> Centro Comercial Apumanque, local 474, Nivel Manquehue, calle A, Las Condes (Metro Manquehue)</a> (Ver en <a href="#" title="titulo" class="font-color-grey-darkest">GoogleMaps</a> - <a href="#" title="titulo" class="font-color-grey-darkest">Waze</a>)
        </p>
        <p class="common-box__excerpt">
            <i class="icon-elem icon-elem--phone_iphone font-color-grey-dark" ></i>
            <a href="#" title="titulo" class="font-color-grey-darkest">contacto@hilosdeconciencia.cl</a>
        </p>
    </div>
</article>