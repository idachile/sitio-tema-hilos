<div class="slider" data-module="slider">
							<div class="slider__items" data-role="slider-list">
								<div class="slider__slide" data-role="slider-slide" >
									<img src="images/img/img-product-10-0.jpg" class="cover-img">
								</div>
								<div class="slider__slide" data-role="slider-slide" >
									<img src="images/img/img-product-10-0.jpg" class="cover-img">
								</div>
								<div class="slider__slide" data-role="slider-slide" >
									<img src="images/img/img-product-10-0.jpg" class="cover-img">
								</div>
                                <div class="slider__slide" data-role="slider-slide" >
									<img src="images/img/img-product-10-0.jpg" class="cover-img">
								</div>
							</div>

							<div class="slider__arrows">
								<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>
								<button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>
							</div>

							<div class="slider__thumbnails">
								<button class="slider__thumbnail slider__thumbnail--current" data-role="slider-thumbnail" data-target="0">
									<img src="images/img/img-product-10-0.jpg">
								</button>
								<button class="slider__thumbnail" data-role="slider-thumbnail" data-target="1">
									<img src="images/img/img-product-10-0.jpg">
								</button>
								<button class="slider__thumbnail" data-role="slider-thumbnail" data-target="2">
									<img src="images/img/img-product-10-0.jpg">
								</button>
                                <button class="slider__thumbnail" data-role="slider-thumbnail" data-target="3">
									<img src="images/img/img-product-10-0.jpg">
								</button>
							</div>
						</div>