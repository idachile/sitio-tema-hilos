<h2 class="main-title">
    <a href="#" title="titulo">Productos Destacados</a>
</h2>
<div class="gridle-row">
						<div class="gridle-gr-3 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-1.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Pantalón Ancho</a>
									</h2>

									<p class="common-box__extra">
										<span class="common-box__tag">$119.990</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-3 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-2.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Portamat tira algodón</a>
									</h2>

									<p class="common-box__extra">
										<span class="common-box__tag">$6.900</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-3 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-3.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Portamat tira algodón</a>
									</h2>

									<p class="common-box__extra">
										<span class="common-box__tag">$6.900</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
						<div class="gridle-gr-3 gridle-gr-12@medium">
							<article>
								<figure class="common-box__figure">
									<a href="#" title="titulo">
										<img src="images/img/img-product-4.jpg">
									</a>
								</figure>
								<div class="common-box__body">
									<h2 class="main-title--tiny">
										<a href="#" title="titulo">Portamat tira algodón</a>
									</h2>

									<p class="common-box__extra">
										<span class="common-box__tag">$6.900</span>

										<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right">
											<span>Ver detalles</span>
											<span class="icon-elem icon-elem--chevron_right font-color-black"></span>
										</a>
									</p>
								</div>
							</article>
						</div>
					</div>