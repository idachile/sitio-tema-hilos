
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					<h2 class="main-title">
						Deje su comentario
					</h2>
				</div>

				<div class="gridle-gr-9 gridle-gr-12@medium">
					<form method="post" action="">
          <div class="gridle-row">
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<div class="form-control" data-error-message="Debe ingresar su nombre">
								<label class="form-control__label">Nombre *</label>
								<input name="nombre_comentario" type="text" class="form-control__field" required placeholder="Ingrese su nombre" data-custom-validation="onlyString">
							</div>
						</div>
						<div class="gridle-gr-6 gridle-gr-12@medium">
							<div class="form-control" data-error-message="Debe ingresar un email válido">
								<label class="form-control__label">Correo *(no será publicado)</label>
								<input name="mensaje_comentario" type="email" class="form-control__field" required placeholder="Ingrese su email">
							</div>
						</div>
						<div class="gridle-gr-12 gridle-gr-12@medium">
							<div class="form-control">
								<label class="form-control__label">Comentario *</label>
								<textarea class="form-control__field" id="mensajeform" required placeholder="Ingresa un comentario" rows="5"></textarea>
								<span class="form-control__label font-size-tiny font-color-grey-light">Máx <span id="countpar">500</span> caracteres</span>
							</div>

							<p class="common-box__meta">* Todos los campos son obligatorios</p>
						</div>
						<div class="gridle-gr-12 gridle-gr-12@medium">
							<button type="submit" class="button button--send">Enviar</button>
						</div>
                        	</div>
					</form>
				</div>
			</div>
            <div class="gridle-gr-9 gridle-gr-12@medium comentarios">
								<article class="common-box common-box--horizontal">
									<figure class="common-box__figure">
										<a href="#">
											<img class="thumbnail" src="http://placehold.it/120x120">
										</a>
									</figure>
									<div class="common-box__body">


										<h2 class="common-box__title">
											<a href="#">Juaquín Quesada</a>
										</h2>

										<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere exercitationem earum adipisci odit consequuntur repellat molestias quasi quo possimus quas culpa minima maiores, sunt dignissimos iste. Perspiciatis harum ullam tempore.</p>

								     	<a class="comment float-right" href="#">Responder</a>
									</div>
								</article>
