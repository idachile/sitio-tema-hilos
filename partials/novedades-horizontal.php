<div class="gridle-gr-12">
	<article class="common-box common-box--horizontal common-box--b-bottom no-box-shadow">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-news-7.jpg">
			</a>
		</figure>
		<div class="common-box__body common-box__body--no-border">
			<p class="common-box__meta">Epígrafe de la noticia o artículo</p>
			
			<h2 class="main__title main-title--tiny no-margin">
				<a href="#" title="titulo">Título en una línea</a>
			</h2>
			<p class="common-box__meta"><em>12 de febrero</em></p>
			<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat porta efficitur. In lacinia ac arcu eu viverra.</p>
		</div>
	</article>
</div>
<div class="gridle-gr-12">
	<article class="common-box common-box--horizontal common-box--b-bottom no-box-shadow">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-news-2.jpg">
			</a>
		</figure>
		<div class="common-box__body common-box__body--no-border">
			<p class="common-box__meta">Epígrafe de la noticia o artículo</p>
			
			<h2 class="main__title main-title--tiny no-margin">
				<a href="#" title="titulo">Título en línea</a>
			</h2>
			<p class="common-box__meta"><em>12 de febrero</em></p>
			<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat porta efficitur. In lacinia ac arcu eu viverra.</p>
		</div>
	</article>
</div>
<div class="gridle-gr-12">
	<article class="common-box common-box--horizontal common-box--b-bottom no-box-shadow">
		<figure class="common-box__figure">
			<a href="#" title="titulo">
				<img src="images/img/img-news-8.jpg">
			</a>
		</figure>
		<div class="common-box__body common-box__body--no-border">
		<p class="common-box__meta">Epígrafe de la noticia o artículo</p>

			<h2 class="main__title main-title--tiny no-margin">
				<a href="#" title="titulo">Título en línea</a>
			</h2>
		<p class="common-box__meta"><em>12 de febrero</em></p>
			<p class="common-box__excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus feugiat porta efficitur. In lacinia ac arcu eu viverra.</p>
		</div>
	</article>
</div>