<form method="post" action="" class="form-control__group">
    <input type="text" class="form-control__field" placeholder="Ingresa tu búsqueda" row="7">
	<span class="form-control__group__addon form-control__group__addon--has-button form-control__search" >
	    <button class="button--white button button--full-width button--ghost font-color-grey-dark" ><i class="icon-elem icon-elem--search"></i></button>
	</span>
</form>