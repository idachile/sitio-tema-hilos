<?php
/* Template name: Portada*/
get_header();
?>

<main>
	<section class="horizon__search">
		<div class="gridle-no-gutter">
			<div class="gridle-gr-12 gridle-gr-12@medium gridle-no-gutter">
				<?php
				//TODO: pasar a funcion en function.php
                    $productos_destacados_bool = get_field('habilitar_producto_destacado');

                    $img_bienvenida = get_field('imagen_bienvenida');
                    $txt_bienvenida = get_field('text_bienvenida');
                    $btn_bienvenida = get_field('boton_bienvenida');
                    if ($productos_destacados_bool) {
                        $printer .= '<article class="common-box--fancy">';
                        $printer .=    '<figure class="common-box__figure">';
                        $printer .=         wp_get_attachment_image($img_bienvenida, 'full');
                        $printer .=    '</figure>';
                        $printer .=    '<div class="common-box--body__medium">';
                        $printer .=        '<p class="main-title main-title_center main-title-bienvenida">';
                        $printer .=            '<a href="#" title="Ir a ">'.$txt_bienvenida.'</a>';
                        $printer .=        '</p>';
                        $printer .=        '<a href="#" title="Ir a '.$btn_bienvenida.'" class="button button__index" >';
                        $printer .=             'Ver catálogo';
                        $printer .=        '</a>';
                        $printer .=    '</div>';
                        $printer .= '</article>';

                        echo $printer;
                    }

          ?>
				</div>
			</div>
	</section>

	<section class="horizon horizon__inner bg-lines">
			<?php
					$on_off = get_field('habilitar_productos_destacados');
					if($on_off):
						$args = array('post_type' => 'producto','posts_per_page' => 4);
						echo getProductosDestacados($args,3);
					endif
			?>
	</section>

	<section class="horizon horizon__inner bg-white">
		<div class="container gridle-no-gutter">
			<div class="gridle-row">
				<?php
					$on_off = get_field('habilitar_noticias');
					if($on_off):
						$args = array('post_type' => 'post','posts_per_page' => 3);
						$noticias_array = get_field('noticias');

						if (!empty($noticias_array)):
							$args['post__in'] = $noticias_array;
						endif;

						echo getNoticias($args,6);
						endif
				?>
				<?php echo mapa($args)?>
			</div>
		</div>
	</section>

</main>

<?php include('footer.php'); ?>
