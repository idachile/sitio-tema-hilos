<?php
/*Template name: Contacto*/
get_header();
?>
	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php echo generate_breadcrumbs();?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php get_template_part('partials/searchbar'); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon horizon__inner bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<h1 class="title">
							Contacto
						</h1>
						<p class="common-box__featured">
							Texto de la caja, como máximo tiene una extensión de 140 caracteres Lorem ipsum dolor sit amet, consectetur adipiscing
						</p>
					</div>

					<div class="gridle-gr-6 gridle-gr-12@medium">
						<form data-module="common-form" method="post" action="contacto-respuesta.php">
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<div class="form-control" data-error-message="Debe ingresar su nombre">
									<label class="form-control__label">Nombre *</label>
									<input type="text" class="form-control__field" required placeholder="Ingrese su nombre" data-custom-validation="onlyString">
								</div>
							</div>
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<div class="form-control" data-error-message="Debe ingresar un email válido">
									<label class="form-control__label">Correo *</label>
									<input id="sample-email" type="email" class="form-control__field" required placeholder="Ingrese su email">
								</div>
							</div>
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<div class="form-control" data-error-message="Debe ingresar su numero de teléfono">
									<label class="form-control__label">Teléfono de contacto</label>
									<input type="text" class="form-control__field" required placeholder="09" data-custom-validation="onlyNumbers" data-minlength="3"
									    data-maxlength="10">
								</div>
							</div>
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<div class="form-control">
									<label class="form-control__label">Comentario *</label>
									<textarea class="form-control__field" id="mensajeform" required placeholder="Ingresa un comentario" rows="5"></textarea>
									<span class="form-control__label font-size-tiny font-color-grey-light">Máx <span id="countpar">500</span> caracteres</span>
								</div>

								<p class="common-box__meta">* Campos obligatorios</p>
							</div>
							<div class="gridle-gr-12 gridle-gr-12@medium">
								<button type="submit" class="button button--send button--ghost">Enviar</button>
							</div>
						</form>
					</div>

					<div class="gridle-gr-6 gridle-gr-12@medium">
						<div class="gridle-gr-12 gridle-gr-12@medium">
							<?php get_template_part('partials/googlemap'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

	<?php get_footer(); ?>