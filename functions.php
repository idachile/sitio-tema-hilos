<?php
    //registro thumbnails
    if (function_exists('add_theme_support')) {
        add_theme_support('post-thumbnails');
    }

    //incrustar css
    function incrustrar_css()
    {
        wp_register_style('main_style', get_bloginfo('template_directory') . '/css/main.css');
        wp_register_style('owl_style', get_bloginfo('template_directory') . '/css/owl.carousel.css');
        wp_register_style('owl_style', get_bloginfo('template_directory') . '/css/transition.css');
        wp_register_style('font_droid', 'https://fonts.googleapis.com/css?family=Droid+Serif:400,700');
        wp_register_style('font_serif', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700');
        wp_enqueue_style('main_style');
        wp_enqueue_style('owl_style');
        wp_enqueue_style('transition');
        wp_enqueue_style('font_droid');
        wp_enqueue_style('font_serif');
    }
    add_action('wp_print_styles', 'incrustrar_css');

    //incrustar Scripts
    function incrustar_scripts()
    {
        wp_enqueue_script('main_script', get_stylesheet_directory_uri(). '/scripts/min/main.min.js');
        wp_enqueue_script('search_script', get_stylesheet_directory_uri(). '/scripts/src/search.js');
        wp_enqueue_script('filtro_script', get_stylesheet_directory_uri(). '/scripts/src/filtros.js');
    }
    add_action('wp_enqueue_scripts', 'incrustar_scripts');

    //menu_setup
    add_action('after_setup_theme', 'menu_setup');
    if (! function_exists('menu_setup')) {
        function menu_setup()
        {
            add_theme_support('post-formats', array('aside', 'video', 'image'));
            add_theme_support('custom-header');
            add_theme_support('automatic-feed-links');
            register_nav_menus(array(
          "menutop" => "Este es el menu top del tema",
          "primary" => "Este es el menu principal del tema",
          "secondary" => "Este es el menu secundario del tema",
          "sidebar" => "Este es el menu lateral del tema",
          "footer" => "Este es el menu del footer",
          "footer_secondary" => "Este es el menu secundario del footer",
          "rrss" => "Redes sociales"
        ));
        }
    }

    //print_menutop
    function print_menu($menutitle)
    {
        $menuprinter= '';
        $menuitems = wp_get_nav_menu_items($menutitle);
        if ($menutitle == 'menutop') {
            foreach ($menuitems as $menuitem) {
              if($menuitem->title == 'Blog'){
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" class="top-bar__item top-bar__item--secondary-hover top-bar__item--highlighted" title="Ir a '. $menuitem->title .'">'. ucfirst($menuitem->title) .'</a>';
              }else{
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" class="top-bar__item top-bar__item--secondary-hover" title="Ir a '. $menuitem->title .'">'. ucfirst($menuitem->title) .'</a>';
              }
            }


        } elseif ($menutitle == 'rrss') {
            foreach ($menuitems as $menuitem) {
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" title="Ir a '. $menuitem->title .'" class="hide-on-medium-down social-link social-link--'. strtolower($menuitem->title) .'">'. $menuitem->title .'</a>';
            }
        } elseif ($menutitle == 'footer-rrss') {
            $menuitems = wp_get_nav_menu_items('rrss');
            foreach ($menuitems as $menuitem) {
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" title="Ir a '. $menuitem->title .'" class="social-link social-link--'. strtolower($menuitem->title) .'">'. $menuitem->title .'</a>';
            }
        } elseif ($menutitle == 'primary') {
            $menuitems = group_menu_items($menutitle);
            foreach ($menuitems as $menuitem) {
                //si el item del menu posee 'hijos' (menu_children), imprime dropdown e hijos
                if (isset($menuitem->menu_children) && !empty($menuitem->menu_children)) {
                    $menuprinter .= '<li class="menu-item menu-item--has-submenu" data-role="touch-submenu">';
                    $menuprinter .= '<a href="#" class="menu-item__link" data-role="touch-submenu-deployer"">'. ucfirst($menuitem->title) .'</a>';
                    $menuprinter .= '<ul class="submenu" >';
                    foreach ($menuitem->menu_children as $childitem) {
                        $menuprinter .= '<li class="submenu__item">';
                        $menuprinter .= '<a href="'. ensure_url($childitem->url) .'" title="Ir a '. $childitem->title .'">'. ucfirst($childitem->title) .'</a>';
                        $menuprinter .= '</li>';
                    }
                    $menuprinter .= '</ul>';
                    $menuprinter .= '</li>';
                } else {
                    $menuprinter .= '<li class="menu-item">';
                    $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" title="Ir a '. $menuitem->title .'" class="menu-item__link">'. ucfirst($menuitem->title) .'</a>';
                    $menuprinter .= '</li>';
                }
            }
        } elseif ($menutitle == 'footer') {
            foreach ($menuitems as $menuitem) {
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" class="footer-bar__link" title="Ir a '. $menuitem->title .'">'. ucfirst($menuitem->title) .'</a>';
            }
        } elseif ($menutitle == 'footer_secondary') {
            foreach ($menuitems as $menuitem) {
                $menuprinter .= '<a href="'. ensure_url($menuitem->url) .'" class="footer-bar__link" title="Ir a '. $menuitem->title .'">'. ucfirst($menuitem->title) .'</a>';
            }
        }

        return $menuprinter;
    }

    /**
     * Agrupa los items de un menu en base a su parent_id
     * devuele un array parecido al original de los menus pero con los hijos incluidos
     * @param  string $menu_slug    - Slug del menu
     * @return array
     */
    function group_menu_items($menu_slug)
    {
        $items = wp_get_nav_menu_items($menu_slug);
        $grouped = [];
        $parents = [];
        $children = [];
        foreach ($items as $item) {
            if ($item->menu_item_parent != 0) {
                $children[] = $item;
            } else {
                $parents[] = $item;
            }
        }
        // se recorren los padres
        foreach ($parents as $parent) {
            // por cada padre se recorren los hijos y se almacenan
            $hijos = [];
            $count = 0;

            foreach ($children as $child) {
                if ($child->menu_item_parent == $parent->ID) {
                    $hijos[] = $child;
                    unset($children[$count]);
                }
                $count++;
            }
            $parent->menu_children = $hijos;
            $grouped[] = $parent;
        }
        return $grouped;
    }

    //pritMe
    function printMe($thing)
    {
        echo '<pre>';
        print_r($thing);
        echo '</pre>';
    }

    //ensure_url
    function ensure_url($proto_url, $protocol = 'http')
    {
        // se revisa si es un link interno primero
      if (substr($proto_url, 0, 1) === '/') {
          return $proto_url;
      }
        if (filter_var($proto_url, FILTER_VALIDATE_URL)) {
            return $proto_url;
        } elseif (substr($proto_url, 0, 7) !== 'http://' || substr($proto_url, 0, 7) !== 'https:/') {
            $url = $protocol . '://' . $proto_url;
        }
      // doble chequeo de validacion de URL
      if (! filter_var($url, FILTER_VALIDATE_URL)) {
          return '';
      }
        return $url;
    }

    //Cortar texto
    //Requiere texto a cortar y cantidad de caracteres
    function cut_string_to($string, $charnum, $sufix = ' ...')
    {
        $string = strip_tags($string);
        if (strlen($string) > $charnum) {
            $string = substr($string, 0, ($charnum - strlen($sufix))) . $sufix;
        }
        return mb_convert_encoding($string, "UTF-8");
        $string = "";
    }

    /**
     * Genera el HTML de los breadcrumbs
     * @return string - HTML de los breadcrumbs
     */
    function get_breadcrumbs()
    {
        global $post, $wp_query;
        $items = '';
        // link al inicio siempre presente
        $items .= '<a href="'. home_url() .'" title="Ir a la página de inicio" class="sub-menu__link" rel="index">Inicio</a>';
        // en los singles y singulars siempre va el titulo al final
        if (is_singular()) {
            if (is_page() || is_single()) {
                $ancestors = get_post_ancestors($post);
                $ancestors = array_reverse($ancestors);
                if (!empty($ancestors)) {
                    foreach ($ancestors as $parent_id) {
                        $items .= '<a href="'. get_permalink($parent_id) .'" title="Ir a '. get_the_title($parent_id) .'" rel="section" class="sub-menu__link">'. get_the_title($parent_id) .'</a>';
                    }
                }
            }
            $items .= '<i class="icon-elem icon-elem--chevron_right" ></i>';
            $items .= '<a href="'. get_the_permalink() .'" class="font-color-grey-dark sub-menu__link--current">'. get_the_title() .'</a>';
        }
        // archivos de custom post types
        elseif (is_archive()) {
            $parent = get_page_by_path('categorias');
            $items .= '<i class="icon-elem icon-elem--chevron_right" ></i>';
            $items .= '<a href="'. get_permalink($parent->ID) .'" title="Ir a '. get_the_title($parent->ID) .'" rel="section" class="sub-menu__link--current">'. get_the_title($parent->ID) .'</a>';
            $post_type = get_post_type_object(get_query_var('post_type'));
            $items .= '<span>'. $post_type->label .'</span>';
        }
        // // archivo de posts
        // elseif( is_home() ){
        //     $parent = get_page_by_path('actualidad');
        //     $items .= '<i class="icon-elem icon-elem--chevron_right" ></i>';
        //     $items .= '<a href="'. get_permalink( $parent->ID ) .'" title="Ir a '. get_the_title( $parent->ID ) .'" rel="section" class="sub-menu__link--current">'. get_the_title( $parent->ID ) .'</a>';
        //     $items .= '<span>Novedades del centro</span>';
        // }
        elseif (is_search()) {
            $items .= '<i class="icon-elem icon-elem--chevron_right" ></i>';
            $items .= '<span>Resultados de búsqueda</span>';
        } elseif (is_sticky(480)) {
            $items .= '<span>HOLA</span>';
        }

        $out = '<p class="sub-menu__holder">';
        $out .= $items;
        $out .= '</p>';
        return $out;
    }

    //image sizes
    add_image_size("size_737x570", 737, 570, true);
    add_image_size("size_525x284", 525, 250, true);
    add_image_size("size_300x300", 300, 300, true);
    add_image_size("size_792x446", 792, 446, true);
    add_image_size("size_120x120", 120, 120, true);
    add_image_size("size_320x180", 320, 180, true);
    add_image_size("size_180x135", 180, 135, true);
    add_image_size("size_348x288", 348, 288, true);
    add_image_size("size_588x330", 588, 330, true);
    add_image_size("size_160x100", 160, 100, true);
    add_image_size("size_282x212", 282, 212, true);
    add_image_size("size_282x282", 282, 282, true);
    add_image_size("size_107x155", 107, 155, true);
    add_image_size("size_1200x500", 1200, 500, true);

    //formateo a peso chileno
    function precioch($number)
    {
        $precio = number_format($number, 0, ',', '.');
        $precio = '$' . $precio;
        return $precio;
    }

    function getNoticias($args,$ancho_columna){
    $titulo = get_field('titulo_noticias');
    $printer = '';

    $noticias = new WP_Query($args);
    while($noticias->have_posts()):
      $noticias->the_post();

      $printer .= '<div class="gridle-row">';
      $printer .=   '<div class="gridle-gr-12">';
      $printer .=     '<article class="common-box common-box--horizontal common-box--b-bottom no-box-shadow">';
      $printer .=       '<figure class="common-box__figure">';
      $printer .=         '<a href="'.get_the_permalink($noticias->ID).'">'.get_the_post_thumbnail($noticias->ID).'</a>';
      $printer .=       '</figure>';
      $printer .=       '<div class="common-box__body common-box__body--no-border">';
      $printer .=         '<p class="common-box__meta">Epigrafe de la noticia</p>';
      $printer .=         '<h2 class="main__title main-title--tiny no-margin">';
      $printer .=           '<a href="'.get_the_permalink($noticias->ID).'">'.get_the_title($noticias->ID).'</a>';
      $printer .=         '</h2>';
      $printer .=         '<p class="common-box__meta">';
      $printer .=           '<em>'.printDate(get_the_date($noticias->ID)).'</em>';
      $printer .=         '</p>';
      $printer .=         '<p class="common-box__excerpt">'.cut_string_to(get_the_excerpt($noticias->ID),100).'</p>';
      $printer .=       '</div>';
      $printer .=     '</article>';
      $printer .=   '</div>';
      $printer .= '</div>';


    endwhile;

    $horizon .=      '<div class="gridle-gr-'.$ancho_columna.' gridle-gr-12@medium">';
    $horizon .=         '<h2>'.$titulo.'</h2>';
    $horizon .=         $printer;
    $horizon .=      '</div>';

    wp_reset_query();
    return $horizon;
    }

function mapa($args){

  $printer .= '<div class="gridle-gr-6 gridle-gr-12@medium">';

  $printer .=     '<h2 class="main-title">';
  $printer .=         '<a href="#" title="#">'.ucfirst($titulo_mapa).'</a>';
  $printer .=     '</h2>';
  $printer .=     '<div class="gridle-row">';
  $printer .=         '<div class="gridle-gr-12 gridle-gr-12@medium">';
  $printer .=           '<article>';
  $printer .=             '<div class="common-box__header">';
  $printer .=               '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13322.103223786084!2d-70.5673523!3d-33.4095335!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8ddbdafb171da67f!2sHilos+de+Conciencia!5e0!3m2!1ses!2scl!4v1496258446094" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';
  $printer .=             '</div>';
  $printer .=             '<div class="common-box__body">';
  $printer .=               '<p class="common-box__excerpt">';
  $printer .=                 '<a href="#"><i class="icon-elem icon-elem--place"></i>';
  $printer .=                 'Centro Comercial Apumanque, local 474, Nivel Manquehue, calle A, Las Condes (Metro Manquehue)</a>';
  $printer .=               '</p>';
  $printer .=               '<p class="common-box__excerpt">';
  $printer .=                 '<a href="#"><i class="icon-elem icon-elem--phone_iphone"></i>';
  $printer .=                 'contacto@hilosdeconciencia.cl</a>';
  $printer .=               '</p>';
  $printer .=             '</div>';
  $printer .=           '</article>';
  $printer .=         '</div>';
  $printer .=     '</div>';
  $printer .= '</div>';
  return $printer;
    }
//TODO: funcion relacionados
    function getProductosRelacionados($args)
    {
        $printer = $horizon = '';

        return $horizon;
    }

    function getThumbSlider()
    {
        $imagenes = get_field('imagen_slide');
        $descripcion_on = get_field('habilitar_descripcion');
        $descripcion_slider = get_field('descripcion_slide');
        $printer = '';
        $current_thumb = ' slider__thumbnail--current';
        foreach ($imagenes as $key => $imagen) {
            $printslide .= $printer;
            $printslide .= '<div class="slider__slide" data-role="slider-slide" data-index="'.$key.'" data-offset="0" style="width: 1280px; float: left; transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">';
            $printslide .=     '<article class="horizon common-box common-box--horizontal common-box--featured no-box-shadow">';
            $printslide .=         '<figure class="common-box__figure">';
            $printslide .=             '<a href="#">';
            $printslide .=                 '<img src="'.$imagen["url"].'" alt="'.$imagen["alt"].'">';
            $printslide .=             '</a>';
            $printslide .=         '</figure>';
            $printslide .=      '</article>';
            $printslide .= '</div>';
            //thumbnails
            $printhumb .= $printbull;
            if ($key == 0) {
                $printhumb .= '<button class="slider__thumbnail'.$current_thumb.'" data-role="slider-thumbnail" data-target="'.$key.'">';
            } else {
                $printhumb .= '<button class="slider__thumbnail" data-role="slider-thumbnail" data-target="'.$key.'">';
            }
            $printhumb .= '<img src="'.$imagen["url"].'">';
            $printhumb .= '</button>';
        }
        $printer .='<div class="slider" data-module="slider" style="visibility: visible; backface-visibility: hidden; perspective: 1000px;">';
        $printer .=     '<div class="slider__items" data-role="slider-list" style="width: 3840px;">';
        $printer .=         $printslide;
        $printer .=     '</div>';
        $printer .=     '<div class="slider__arrows">';
        $printer .=         '<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>';
        $printer .=         '<button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>';
        $printer .=     '</div>';
        $printer .=     '<div class="slider__thumbnails">';
        $printer .=         $printhumb;
        $printer .=     '</div>';
        $printer .= '</div>';
        if($descripcion_on == true){
          $printer .= '<article>'.$descripcion_slider.'</article>';
        }
        return $printer;
    }


function getProductosDestacados($args,$ancho_columna)
{
    $query_destacados = new WP_Query($args);
    $printer = '';
    while ($query_destacados->have_posts()) :
        $query_destacados->the_post();

    $printer .='<div class="gridle-gr-'.$ancho_columna.' gridle-gr-12@medium">';
        //imagen del producto
        $printer .= '<article class="producto">';
    $printer .=     '<figure class="common-box__figure">';
    $printer .=         '<a href="#" title="titulo">';
    $printer .=             get_the_post_thumbnail($post->ID, 'full');
    $printer .=         '</a>';
    $printer .=     '</figure>';
        //contenido del producto
        $printer .=     '<div class="common-box__body">';
    $printer .=          '<h2 class="main-title--tiny">';
    $printer .=               '<a href="'.get_the_permalink($post->ID).'" title="titulo">'.get_the_title($post->ID).'</a>';
    $printer .=          '</h2>';
    $printer .=           '<p class="common-box__extra">';
    $printer .=                '<span class="common-box__tag">'.precioch(get_field('precio', $post->ID)).'</span>';
    $printer .=                '<a href="'.get_the_permalink($post->ID).'" title="titulo" class="button button--black__small button--small button--ghost float-right">';
    $printer .=                     '<span>Ver detalles</span>';
    $printer .=                         '<span>';
    $printer .=                             '<i class="icon-elem icon-elem--chevron_right font-colo-black"></i>';
    $printer .=                         '</span>';
    $printer .=                     '</span>';
    $printer .=                 '</a>';
    $printer .=             '</p>';
    $printer .=     '</div>';
    $printer .= '</article>';
    $printer .='</div>';

    endwhile;


    $horizon .= '<div class="container gridle-no-gutter">';
    $horizon .=     '<div class="gridle-row">';
    $horizon .=         '<div class="gridle-gr-12 gridle-gr-12@medium">';
    $horizon .=             '<h2>Productos Destacados</h2>';
    $horizon .=             '<div class="gridle-row">';
    $horizon .=                 $printer;
    $horizon .=             '</div>';
    $horizon .=         '</div>';
    $horizon .=     '</div>';
    $horizon .= '</div>';
    wp_reset_query();
    return $horizon;
}

function googlemap($direccion)
{
    $gmap = '<article>
                    <div class="common-box__header">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13322.103223786084!2d-70.5673523!3d-33.4095335!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8ddbdafb171da67f!2sHilos+de+Conciencia!5e0!3m2!1ses!2scl!4v1496258446094" width="400" height="420" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="common-box__body">
                        <p class="common-box__excerpt">
                            <i class="icon-elem icon-elem--place font-color-grey-dark" ></i>
                            <span class="vcard">
                                <a href="#" title="titulo" class="font-color-grey-darkest"><span class="steet-adress">Suecia 1709</span>, <span class="locality">Providencia</span></a>
                            </span> (Ver en <a href="#" title="titulo" class="font-color-grey-darkest">GoogleMaps</a> - <a href="#" title="titulo" class="font-color-grey-darkest">Waze</a>)
                        </p>
                        <p class="common-box__excerpt">
                            <i class="icon-elem icon-elem--phone_iphone font-color-grey-dark" ></i>
                            <a href="tel:(+56) 2 2356270" title="titulo" class="font-color-grey-darkest"><span class="tel">+56 2 2356270</span></a>
                        </p>
                    </div>
                </article>';

    return $gmap;
}



    /**
    * Devuelve el HTML de la paginacion de una $wp_query
    * @param  object $query $wp_query a la cual paginar
    * @param  string  $prev  Texto para el boton anterior
    * @param  string  $next  Texto para el boton siguiente
    * @return string HTML de la paginacion
    */
    function get_pagination($query = false)
    {
        global $wp_query;
        if (!$query) {
            $query = $wp_query;
        }
        $query -> query_vars['paged'] > 1 ? $current = $query -> query_vars['paged'] : $current = 1;

        // opciones generales para los links de paginacion, la opcion "format" puede esar en español
        // solo si es que esta activo el filtro para cambiar esto
        $pagination = array(
            'base' => @add_query_arg('paged', '%#%'),
            'format' => '/pagina/%#%',
            'total' => $query -> max_num_pages,
            'current' => $current,
            'prev_text' => __($prev),
            'next_text' => __($next),
            'mid_size' => 2,
            'type' => 'array'
        );

        $items = "";
        $pageLinksArray = paginate_links($pagination);
        $out = '';

        if (!empty($pageLinksArray)) {
            // reviso si es que existe un link "anterior", lo saco del array y lo guardo en variable
            $prevLink = '';
            if (preg_match('/'.$prev.
                    '/i', $pageLinksArray[0])) {
                $prevLink = preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="pagination__arrow--prev" rel="prev" ', array_shift($pageLinksArray));
                // $prevLink = preg_replace('/(?<=\"\>)(.*?)(?=\<\/)/', '&lsaquo;', $prevLink);
            }

            // lo mismo para el link "siguiente"
            $nextLink = '';
            if (preg_match('/'.$next.
                    '/i', end($pageLinksArray))) {
                $nextLink = preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="pagination__arrow--next" rel="next" ', array_pop($pageLinksArray));
                // $nextLink = preg_replace('/(?<=\"\>)(.*?)(?=\<\/)/', '&rsaquo;', $nextLink);
            }

            // se ponen los links "anterior" y "siguiente" dentro del html deseado

            //$items .= $prevLink;
            //se itera sobre los links de paginas
            foreach ((array) $pageLinksArray as $pageLink) {
                // se itera sobre el resto de los links con el fin de cambiar y/o agregar clases personalizadas

                // si estoy en la pagina
                if (preg_match('/current/i', $pageLink)) {
                    $items .= '<li><a href="#" class="pagination--current">'.preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' rel="nofollow" ', $pageLink.
                        '</a></li>');
                }

                // si son los puntitos
                elseif (preg_match('/dots/i', $pageLink)) {
                    $items .= '<li>'.preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' class="page-link page-num dots" rel="nofollow" ', $pageLink.
                        '</li>');
                }

                // se cambian las clases de los links
                else {
                    $items .= '<li>'.preg_replace('/\sclass=[\'|"][^\'"]+[\'|"]/', ' rel="nofollow" title="Ir a la página" ', $pageLink.
                        '</li>');
                }
            }
            //$items .= $nextLink;

            $out = '<div class="pagination"><ul>'.$prevLink.$items.$nextLink.
            '</ul></div>';
        }

        return $out;
    }

    //imprimir fecha como '1 de Enero'
    function printDate($date)
    {
        $postDate .= date_i18n('d', strtotime($date));
        $postDate .= ' de ';
        $postDate .= ucfirst(date_i18n('F', strtotime($date)));
        return $postDate;
    }

    //author name
    function printAuthorname($authorid)
    {
        $authorname = get_user_by('ID', $authorid);
        return $authorname->display_name;
    }

    //imprimir categorias inline
    function printTagsInline($postid)
    {
        $tags = get_the_tags($postid);
        $tagslength = sizeof($tags) - 1;
        for ($x=0; $x <= $tagslength; $x++) {
            if ($x == $tagslength) {
                $tagprinter .= $tags[$x]->name;
            } else {
                $tagprinter .= $tags[$x]->name . ', ';
            }
        }
        return $tagprinter;
    }

function getVideo()
{
  //se toman los datos de los field en wordpress
  $url_video = get_field('url_video');
  $descripcion_video = get_field('descripcion_video');
  //se corta la el url del video para tener solo el codigo del mismo
  $codigo_video = substr($url_video, 32);
  //se arma la estructura html con la variables que contienen los datos

    $printer .='<iframe width="960" height="620" src="https://www.youtube.com/embed/';
    $printer .=       $codigo_video;
    $printer .='" frameborder="0" allowfullscreen></iframe>';
    $printer .='<article>'.$descripcion_video.'</article>';

    return $printer;
}

function getDapartamentos($args)
{
    $departamentos_getgfield = get_field('departamento');
    $printer = '';
    $query_dept = new WP_Query($args);

    while ($query_dept->have_posts()) :
        $query_dept->the_post();

    $printer .=         '<article class="common-box common-box--horizontal common-box--b-bottom">';
    $printer .=             '<div class="gridle-row">';
    $printer .=                 '<div class="gridle-gr-6 gridle-gr-12@medium no-gutter-top-bottom">';
    $printer .=                    '<figure class="common-box__figure">';
    $printer .=                        '<a href="#" title="titulo">';
    $printer .=                            get_the_post_thumbnail($post->ID, 'full');
    $printer .=                        '</a>';
    $printer .=                    '</figure>';
    $printer .=                 '</div>';
    $printer .=                '<div class="gridle-gr-6 gridle-gr-12@medium">';
    $printer .=                    '<div class="common-box__body common-box__body--no-border">';
    $printer .=                        '<p class="common-box__meta no-padding">' . $departamento["epigrafe_departamento"] . '</p>';
    $printer .=                         '<h2 class="main-title">';
    $printer .=                             '<a href="'.get_the_permalink($post->ID).'/'.$post->post_name.'" title="titulo">'.get_the_title($post->ID).'</a>';
    $printer .=                        '</h2>';
    $printer .=                        '<p class="common-box__excerpt font-size-medium">' .get_the_content($post->ID) . '</p>';
    $printer .=                    '</div>';
    $printer .=                '</div>';
    $printer .=             '</div>';
    $printer .=         '</article>';
    endwhile;
    $horizon .= '<section class="horizon horizon__inner bg-lines vestimenta">';
    $horizon .=     '<div class="container gridle-no-gutter">';
    $horizon .=         $printer;
    $horizon .=     '</div>';
    $horizon .= '</section>';
    wp_reset_query();
    return $horizon;
}

function getListadoProductos($args)
{

  $filtros_tipo = get_terms(array(
  'taxonomy' => 'categoria_producto',
  'child_of' => 52

));

  $filtros_talla = get_terms('talla');
  $filtros_yoga = get_terms('yoga');

  foreach($filtros_yoga as $yoga){}

    $printer = '';
    $productos = new WP_Query($args);
    if (have_posts()) {
        while ($productos->have_posts()) :
        $productos->the_post();
            //imagen
            $printer .= '<div class="gridle-gr-4 gridle-gr-12@medium">';
        $printer .=     '<article class="producto">';
        $printer .=         '<figure class="common-box__figure">';
        $printer .=             '<a href="'.get_the_permalink($post->ID).'" title="titulo">';
        $printer .=                 get_the_post_thumbnail($post->ID, 'full');
        $printer .=             '</a>';
        $printer .=         '</figure>';
            //contenido
            $printer .=         '<div class="common-box__body">';
        $printer .=             '<h2 class="main-title--tiny">';
        $printer .=                 '<a href="'.get_the_permalink($post->ID).'" title="titulo">'.get_the_title($post->ID).'</a>';
        $printer .=             '</h2>';
        $printer .=             '<p class="common-box__extra">';
        $printer .=                 '<span class="common-box__tag">'.precioch(get_field('precio', $post->ID)).'</span>';
        $printer .=                 '<a href="'.get_the_permalink($post->ID).'" title="titulo" class="button button--black__small button--small button--ghost float-right">';
        $printer .=                     '<span>Ver detalles</span>';
        $printer .=                     '<span class="icon-elem icon-elem--chevron_right font-color-black"></span>';
        $printer .=                 '</a>';
        $printer .=             '</p>';
        $printer .=         '</div>';
        $printer .=     '</article>';
        $printer .= '</div>';
        endwhile;
    } else {
        $printer .= '<h3 class="horizon__inner">Producto no disponible</h3>';
    }
    wp_reset_query();
    return $printer;
}

////////////////////////
//FUNCIONES PARA LA PAGINA DE PRODUCTOS
////////////////////////

function getSku()
{
    $sku = get_field('sku_producto');

    $printer = "";
    $printer .= '<strong>SKU: ' . $sku . '</strong>';

    return $printer;
}

function getStock()
{
    $stock = get_field('stock_producto');
    $printer = "";
    $printer .=  $stock;
    return $printer;
}

function getTallas()
{

   $tallas = get_terms('talla');
    foreach ($tallas as $key => $talla) :
        if ($talla) {
            $printer .= '<span class="common-box__details common-box__details--border">'.strtoupper($talla->slug).'</span>';
        } else {
            $printer .= '<span class="common-box__details">'.strtoupper($talla->slug).'</span>';
        }
    endforeach;
    $printer .= '<a href="#" title="titulo" data-module="modal" data-role="modal-btn" data-target="';
    $printer .= get_template_directory_uri() . '/partials/ajax/ajax-template-guia-talla.html';
    $printer .= '">Guía de talla</a>';
    wp_reset_query();
    return $printer;
}

function getVariedades()
{
    if (have_rows('variedad_disponible')):
    $printer = '';
    $printer .=     '<select class="form-control__field form-control__field--select grey-color_dark">';
    while (have_rows('variedad_disponible')):the_row();
    $variedad = get_sub_field('variedad_producto');
    $printoptions .= '<option value="'.$variedad .'">'.ucwords($variedad).'</option>';
    endwhile;
    endif;
    $printer .= $printoptions;
    $printer .=     '</select>';

    return $printer;
}

function getPrecio()
{
    $precio = get_field('precio');
    return precioch($precio);
}

function getFichaTecnica()
{
    if (have_rows('ficha_tecnica')) :
    $printer = '';
    $printer .= '<ul>';
    while (have_rows('ficha_tecnica')):the_row();
    $caracteristicas = get_sub_field('caracteristica');

    $printfic .= '<li>';
    $printfic .=    $caracteristicas;
    $printfic .= '</li>';

    endwhile;
    endif;
    $printer .= $printfic;
    $printer .= '</ul>';

    return $printer;
}

function getInfoSlider()
{
    $imagenes = get_field('imagen_slide');
    $printer = '';
    $printbull = '';
    $current = 'slider__bullet--current';
    foreach ($imagenes as $key => $imagen) {
        $printslide .= $printer;
        $printslide .= '<div class="slider__slide" data-role="slider-slide" data-index="'.$key.'" data-offset="0" style="width: 1280px; float: left; transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">';
        $printslide .=     '<article class="horizon common-box common-box--horizontal common-box--featured common-box--b-bottom no-box-shadow">';
        $printslide .=         '<figure class="common-box__figure">';
        $printslide .=             '<a href="#">';
        $printslide .=                 '<img src="'.$imagen["url"].'" alt="'.$imagen["alt"].'">';
        $printslide .=             '</a>';
        $printslide .=         '</figure>';
        $printslide .=      '<div class="common-box__body common-box__body--no-border">';
        $printslide .=         '<p class="common-box__meta">'.$imagen["caption"].'</p>';
        $printslide .=         '<h2 class="common-box__title">';
        $printslide .=             '<a href="#">'.$imagen["title"].'</a>';
        $printslide .=          '</h2>';
        $printslide .=          '<p class="common-box__meta">';
        $printslide .=             '<em>'.$imagen["date"].', por</em>';
        $printslide .=             '<a href="#">'.$imagen["author"].'</a>';
        $printslide .=          '</p>';
        $printslide .=         '<p class="common-box__meta">';
        $printslide .=             '<a href="#">Categoría 1</a>, <a href="#">Categoría 2</a>';
        $printslide .=          '</p>';
        $printslide .=          '<p class="common-box__excerpt">'.$imagen["description"].'</p>';
        $printslide .=        '</div>';
        $printslide .=      '</article>';
        $printslide .= '</div>';
        //bullets
        $printbullet .= $printbull;
        if ($key == 0) {
            $printbullet .= '<button class="slider__bullet '.$current.'" data-role="slider-bullet" data-target="'.$key.'"></button>';
        } else {
            $printbullet .= '<button class="slider__bullet" data-role="slider-bullet" data-target="'.$key.'"></button>';
        }
    }
    $printer .='<div class="slider" data-module="slider" style="visibility: visible; backface-visibility: hidden; perspective: 1000px;">';
    $printer .=     '<div class="slider__items" data-role="slider-list" style="width: 3840px;">';
    $printer .=         $printslide;
    $printer .=     '</div>';
    $printer .=     '<div class="slider__bullets font-centered">';
    $printer .=         $printbullet;
    $printer .=     '</div>';
    $printer .= '</div>';
    return $printer;
}

function getSlider()
{
    $imagenes = get_field('imagen_slide');
    $printer = '';
    $printbull = '';
    $current = 'slider__bullet--current';
    foreach ($imagenes as $key => $imagen) {
        $printslide .= $printer;
        $printslide .= '<div class="slider__slide" data-role="slider-slide" data-index="'.$key.'" data-offset="0" style="width: 1280px; float: left; transition-duration: 0ms; transform: translate(0px, 0px) translateZ(0px);">';
        $printslide .=     '<article class="horizon common-box common-box--horizontal common-box--featured common-box--b-bottom no-box-shadow">';
        $printslide .=         '<figure class="common-box__figure">';
        $printslide .=             '<a href="#">';
        $printslide .=                 '<img src="'.$imagen["url"].'" alt="'.$imagen["alt"].'">';
        $printslide .=             '</a>';
        $printslide .=         '</figure>';
        $printslide .=      '</article>';
        $printslide .= '</div>';
        //bullets
        $printbullet .= $printbull;
        if ($key == 0) {
            $printbullet .= '<button class="slider__bullet '.$current.'" data-role="slider-bullet" data-target="'.$key.'"></button>';
        } else {
            $printbullet .= '<button class="slider__bullet" data-role="slider-bullet" data-target="'.$key.'"></button>';
        }
    }
    $printer .='<div class="slider" data-module="slider" style="visibility: visible; backface-visibility: hidden; perspective: 1000px;">';
    $printer .=     '<div class="slider__items" data-role="slider-list" style="width: 3840px;">';
    $printer .=         $printslide;
    $printer .=     '</div>';
    $printer .=     '<div class="slider__bullets font-centered">';
    $printer .=         $printbullet;
    $printer .=     '</div>';
    $printer .= '</div>';
    return $printer;
}

function getSidebar()
{
    $sidebar = get_field('tipo_sidebar');
    $printer = '';
    if ($sidebar == 'newsletter') {
        $printside = $printer;
        $printside .= '<div class="main_color_dark_bg">';
        $printside .=    '<h2>suscríbrete al newsletter</h2>';
        $printside .=    '<p>Suscríbete a nuestro newsletter y recibe semanalmente en tu email todas las novedades de Lorem Ipsum Dolor.</p>';
        $printside .=    '<form method="post" action="" class="form-control__group">';
        $printside .=       '<div class="form-control">';
        $printside .=           '<label class="form-control__label" for="nombre_newsletter">Nombre</label>';
        $printside .=           '<input type="text" class="form-control__field" name="nombre_newsletter" placeholder="Ingrese su nombre"/>';
        $printside .=       '</div>';
        $printside .=       '<div class="form-control">';
        $printside .=       '<label class="form-control__label" for="correo_newsletter">Correo</label>';
        $printside .=       '<input type="text" class="form-control__field" name="correo_newsletter" placeholder="Ingrese su correo"/>';
        $printside .=       '</div>';
        $printside .=       '<input class="button button--extralarge" type="submit" value="suscribirme ahora">';
        $printside .=     '</form>';
        $printside .= '</div>';
    } else {
        $printside .= '<article class="content-box--grey content__sidebar">';
        $printside .=   '<div class="common-box__body">';
        $printside .=       '<p class="common-box__excerpt font-size-regular">Te invitamos a navegar dentro de <strong>nuestras categorías</strong></p>';
        $printside .=        '<ul>';
        $printside .=        $categorias = get_categories();
        foreach ($categorias as $categ) {
            if ($categ->slug != 'sin-categoria') {
                $out .= '<li>';
                $out .=    '<a href="/categorias/'.$categ->slug.'" title="titulo" class="font-color-grey-darkest font-size-regular">';
                $out .=        $categ->name;
                $out .=    '</a>';
                $out .= '</li>';
            }
        }
        $printside .= $out;
        $printside .=         '</ul>';
        $printside .=      '</div>';
        $printside .= '</article>';
    }
    return $printside;
}
//GALERIA GENERAL
function getGaleria()
{
    $galerias = get_field('galeria_general');
    $printer = '';
    foreach ($galerias as $key => $galeria) {
        $printgal .=    '<div class="slider__slide" data-role="slider-slide" data-index="'.$key.' "backface-visibility: hidden; perspective: 1000px;';
        $printgal .=        'data-offset="-624" style="width: 624px; float: left; transition-duration: 300ms; transform: translate(-624px, 0px) translateZ(0px);">';
        $printgal .=        '<img src="'.$galeria['url'].'" class="cover-img">';
        $printgal .=    '</div>';
        //thumbnails
        $printhumb .=    '<button class="slider__thumbnail slider__thumbnail--current" data-role="slider-thumbnail" data-target="'.$key.'">';
        $printhumb .=        '<img src="'.$galeria['url'].'">';
        $printhumb .=    '</button>';
    }

    $printer .= '<div class="slider" data-module="slider">';
    $printer .=     '<div class="slider__items" data-role="slider-list">';
    $printer .=         $printgal;
    $printer .=     '</div>';
    $printer .=     '<div class="slider__arrows">';
    $printer .=         '<button class="slider__arrow slider__arrow--prev" data-role="slider-arrow" data-direction="prev"></button>';
    $printer .=         '<button class="slider__arrow slider__arrow--next" data-role="slider-arrow" data-direction="next"></button>';
    $printer .=     '</div>';

    $printer .=     '<div class="slider__thumbnails">';
    $printer .=         $printhumb;
    $printer .=     '</div>';

    $printer .= '</div>';

    return $printer;
}

function getNoticiaDestacada($args)
{

    $printer = '';
    $noticia_destacada = new WP_Query($args);
    while ($noticia_destacada->have_posts()) {
        $noticia_destacada->the_post();

        $printer .= '<article class="common-box common-box--featured">';
        $printer .=     '<div class="common-box__body">';
        $printer .=         '<p class="common-box__meta">Epigrafe de la noticia</p>';
        $printer .=         '<h2 class="common-box__title">';
        $printer .=             get_the_title($post->ID);
        $printer .=         '</h2>';
        $printer .=         '<p class="common-box__meta">';
        $printer .=           '<em>'.printDate(get_the_date($noticias->ID)).'</em>';
        $printer .=         '</p>';
        $printer .=     '</div>';
        $printer .=     '<figure class="common-box__figure">';
        $printer .=        '<a href="#">';
        $printer .=             '<img src="'.get_the_post_thumbnail_url($post->ID, 'full').'">';
        $printer .=         '</a>';
        $printer .=     '</figure>';
        $printer .= '</article>';
        $printer .= wpautop(get_the_content($post->ID));
    }
    wp_reset_query();
    return $printer;
}

// filtro mejorados
function getFiltroProducto()
{
    $filtros_tipo = get_terms(array(
    'taxonomy' => 'categoria_producto',
    'child_of' => 52

));

    $filtros_talla = get_terms('talla');
    $filtros_yoga = get_terms('yoga');


    $printer = '';
    $prininp.= '<label class="form-control__label title-category-filter">Categorías</label>';
    foreach ($filtros_tipo as $filtro_tipo) {
        $prininp .=     '<label class="form-switch form-switch--block">';
        $prininp .=         '<input type="checkbox" name="'. strtolower($filtro_tipo->name).'" value="'. strtolower($filtro_tipo->name).'"> ';
        $prininp .=         '<span class="form-switch__input form-switch__input--checkbox"></span>';
        $prininp .=         '<span class="form-switch__label form-switch__label--marglef">' . ucwords($filtro_tipo->name). '</span>';
        $prininp .=     '</label>';
    }
    $prininp.= '<label class="form-control__label title-category-filter">Tallas</label>';
    foreach ($filtros_talla as $filtro_talla) {
        $prininp .=     '<label class="form-switch form-switch--block">';
        $prininp .=         '<input type="checkbox" name="'. strtolower($filtro_talla->name).'"> ';
        $prininp .=         '<span class="form-switch__input form-switch__input--checkbox"></span>';
        $prininp .=         '<span class="form-switch__label form-switch__label--marglef">' . strtoupper($filtro_talla->name). '</span>';
        $prininp .=     '</label>';
    }
    $prininp.= '<label class="form-control__label title-category-filter">Yoga</label>';
    foreach ($filtros_yoga as $filtro_yoga) {
        $prininp .=     '<label class="form-switch form-switch--block">';
        $prininp .=         '<input type="checkbox" name="'. strtolower($filtro_yoga->name).'" value="'. strtolower($filtro_yoga->name).'"> ';
        $prininp .=         '<span class="form-switch__input form-switch__input--checkbox"></span>';
        $prininp .=         '<span class="form-switch__label form-switch__label--marglef">' . ucwords($filtro_yoga->name). '</span>';
        $prininp .=     '</label>';
    }

    $printer .= '<form method="GET" action class="form-control__group filter">';
    $printer .=     '<h4>Filtros</h4>';
    $printer .=         $prininp;
    $printer .=     '<input class="button button--extralarge" type="submit" value="Filtrar"/>';
    $printer .= '</form>';
    return $printer;


    $printer = '<div class="gridle-gr-9 gridle-gr-12@medium">';

    $printer = '</div>';
}

// PRUEBA DE OPCIONES DE PANTALLA PRODUCTOS (cambiar a directorio admin)
//ID
function posts_columns_id($defaults)
{
    //nombre de la columna en el
$defaults['wps_post_id'] = __('ID');
    return $defaults;
}
function posts_custom_id_columns($column_name, $id)
{
    if ($column_name === 'wps_post_id') {
        echo $id;
    }
}
add_filter('manage_posts_columns', 'posts_columns_id', 5);
add_action('manage_posts_custom_column', 'posts_custom_id_columns', 5, 2);
//THUMBNAIL
function producto_column_thumbnail($defaults)
{
    $defaults['posts_thumbs'] = __('Imagen');
    return $defaults;
}

function posts_custom_thumbnail_columns($column_name, $thumbnail)
{
    if ($column_name === 'posts_thumbs') {
        $thumbnail = the_post_thumbnail(array('70','70'));
        echo $thumbnail;
    }
}

add_filter('manage_posts_columns', 'producto_column_thumbnail', 1);
add_action('manage_posts_custom_column', 'posts_custom_thumbnail_columns', 1, 5);

//SKU
function posts_columns_sku($defaults)
{
    //nombre de la columna
$defaults['sku'] = __('SKU');
    return $defaults;
}
function posts_custom_sku_columns($column_name, $sku)
{
    if ($column_name === 'sku') {
        $sku = get_field('sku_producto');
        echo $sku;
    }
}

add_filter('manage_posts_columns', 'posts_columns_sku', 5);
add_action('manage_posts_custom_column', 'posts_custom_sku_columns', 5, 2);

//DISPONIBILIDAD
function posts_columns_disponibilidad($defaults)
{
    //nombre de la columna
$defaults['disponibilidad'] = __('Dispos');
    return $defaults;
}

function posts_custom_disponibilidad_columns($column_name, $disponibilidad)
{
    if ($column_name === 'disponibilidad') {
        $disponibilidad = get_field('disponibilidad_producto');
        echo $disponibilidad;
    }
}

add_filter('manage_posts_columns', 'posts_columns_disponibilidad', 5);
add_action('manage_posts_custom_column', 'posts_custom_disponibilidad_columns', 5, 2);

//DISPONIBILIDAD
function posts_columns_precio($defaults)
{
    //nombre de la columna
$defaults['precio'] = __('Precio');
    return $defaults;
}

function posts_custom_precio_columns($column_name, $precio)
{
    if ($column_name === 'precio') {
        $precio = get_field('precio');
        echo precioch($precio);
    }
}

add_filter('manage_posts_columns', 'posts_columns_precio', -1);
add_action('manage_posts_custom_column', 'posts_custom_precio_columns', 1, 2);

//comentarios

function customComment($comment, $args, $depth){
  //echo '<pre>';
  //var_dump($comment);
  //echo '</pre>';
  $GLOBALS['comment'] = $comment;
  global $post;


  $printer .=   '<article class="common-box common-box--horizontal">';
  $printer .=     '<figure class="common-box__figure">';
  $printer .=       '<a href="">';
  $printer .=         get_avatar(32);
  $printer .=       '</a>';
  $printer .=     '</figure>';
  $printer .=     '<div class="common-box__body">';
  $printer .=       '<h2 class="common-box__title">';
  $printer .=         '<a href="">'.get_comment_author().'</a>';
  $printer .=       '</h2>';
  $printer .=       '<p class="common-box__excerpt">'.get_comment_text().'</p>';
  $printer .=       '<a class="comment float-right">'.get_comment_reply_link().'</a>';
  $printer .=     '</div>';
  $printer .=   '</article>';


  $contenido = get_comment_text();

  echo $printer;
}
