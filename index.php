<?php get_header(); ?>

	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-10 gridle-gr-12@medium gridle-gr-centered">
						<?php get_template_part('partials/searchbar'); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="horizon bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-7 gridle-gr-12@medium">
						<article class="common-box--fancy">
							<figure class="common-box__figure">
								<img src="http://placehold.it/737x570">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title">
									<a href="producto-full.html" >Index.php</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--highlighted">$4.990</span>

									<a href="#" title="titulo" class="button button--more button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
					</div>
					<div class="gridle-gr-5 gridle-gr-12@medium">
						<article class="common-box--fancy common-box--fancy__left">
							<figure class="common-box__figure">
								<img src="http://placehold.it/525x284">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Tadao Berenjena</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--faded common-box__tag--extra">$30.990</span>
									<span class="common-box__tag common-box__tag--highlighted">$19.990</span>

									<a href="#" title="titulo" class="button button--more button--black__right button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
						<article class="common-box--fancy">
							<figure class="common-box__figure">
								<img src="http://placehold.it/525x284">
							</figure>
							<div class="common-box__body">
								<h2 class="main-title--tiny">
									<a href="#" title="titulo" >Dumbo Negro</a>
								</h2>

								<div class="common-box__footer">
									<span class="common-box__tag common-box__tag--faded common-box__tag--extra">$30.990</span>
									<span class="common-box__tag common-box__tag--highlighted">$19.990</span>

									<a href="#" title="titulo" class="button button--more button--black__right button--small button--ghost float-right" >Ver detalles</a>
								</div>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php get_footer(); ?>