<?php
/*Template name: Blog*/
the_post();
get_header();
?>

<main>
	<section class="horizon container">
			<?php echo getInfoSlider() ?>
	</section>

	<section class="horizon horizon__inner bg-white">
		<div class="container">
			<div class="gridle-row">
					<?php
						$on_off = get_field('habilitar_noticias');
						if($on_off):
							$args = array('post_type' => 'post','posts_per_page' => -1);
							$noticias_array = get_field('noticias');

							if (!empty($noticias_array)):
								$args['post__in'] = $noticias_array;
							endif;

							echo getNoticias($args,9);
						endif
					?>
				<div class="gridle-gr-3 gridle-gr-12@medium">

					<?php
						$sidebar = get_field('tipo_sidebar');
						if($sidebar){
							echo getSidebar();
						}
					?>
				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>
