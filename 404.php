<?php
/*Template name: 404*/
get_header();
?>
<main>


	<section class="horizon bg-white">
		<div class="container gridle-no-gutter fondo-yoga">
			<div class="gridle-row">
				<div class="gridle-gr-12 gridle-gr-12@medium">
					
					<h4 class="title title--mask">
						404
					</h4>
					<p class="common-box__meta font-size-regular">página no encontrada</p>
				</div>
			</div>
		</div>
	</section>

	<section class="horizon horizon__inner bg-lines">
		<?php echo getCategoriasDestacadas()?>
	</section>
	
</main>

<?php get_footer(); ?>