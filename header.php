<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>
		<?php bloginfo(name); ?>
		<?php wp_title(); ?>
	</title>
	<?php incrustrar_css(); ?>
	<link href="<?php bloginfo(stylesheet_url); ?>" rel="stylesheet" type="text/css">
	<?php wp_head(); ?>
</head>
<body>
	<header class="header-bar" >
		<section class="top-bar top-bar--secondary">
			<div class="container" data-area-name="menu-top" >
				<div class="top-bar__items" data-mq-change="medium" data-match-area="menu-top@medium" data-unmatch-area="menu-top" >
					<?php
						echo print_menu('menutop');
						echo print_menu('rrss');
					?>
				</div>
			</div>
		</section>

		<nav class="nav-bar" data-module="nav-bar" >
			<div class="container">
				<div class="nav-bar__holder">
					<div class="nav-bar__brand">
						<a class="app-brand app-brand--inline" href="<?php echo home_url(); ?>">
							<img class="app-brand__logo" src="<?php bloginfo('template_directory')?>/images/logos/logo-header.png" alt="Logo" >
						</a>
					</div>
					<div class="nav-bar__body" data-role="nav-body">
						<button class="nav-bar__deploy-button" aria-label="Ver menú" data-role="nav-deployer" >
							<span></span>
						</button>
						<div class="nav-bar__menu-holder" >
							<ul class="nav-bar__menu">
								<?php echo print_menu('primary'); ?>
								<li><a href="#" id="searchtoggl" class="icon-elem icon-elem--search search-boton"></a></li>	
							</ul>
							<div class="nav-bar__auxmenu" data-area-name="menu-top@medium"></div>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
		<section id="searchbar" class="horizon__search horizon__search--main-color-dark">
		<div class="container gridle-row">
				<div class="gridle-gr-7 gridle-gr-centered search-box">
					<?php include('partials/searchbar.php');?>
				</div>
		</div>
	</section>
	<section class="horizon__search horizon__search--main-color-dark ocultar">
		<div class="container">
					<<?php echo get_breadcrumbs();?>
		</div>
	</section>