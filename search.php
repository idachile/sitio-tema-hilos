<?php
get_header();
global $wp_query;
$search_term = sanitize_text_field(get_query_var('s'));
?>
	<main>
		<section class="horizon__search bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<?php echo generate_breadcrumbs();?>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<form method="get" class="form-control__group" data-validate>
							<input autofocus type="text" class="form-control__field" placeholder="Ingresa tu búsqueda" name="s" required value="<?php echo $search_term; ?>">
							<span class="form-control__group__addon form-control__group__addon--has-button form-control__search" >
								<button class="button--white button button--full-width button--ghost font-color-grey-dark" type="submit"><i class="icon-elem icon-elem--search" ></i></button>
							</span>
						</form>
					</div>
				</div>
			</div>
		</section>

		<?php
			$resultados = $wp_query->found_posts;
		?>

		<section class="horizon horizon__inner bg-white horizon">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-12 gridle-gr-12@medium">
						<?php
							if(!$resultados){
								$printer .= '<h2 class="title">';
								$printer .= 	'Lo sentimos';
								$printer .= '</h2>';
								$printer .= '<p class="common-box__excerpt">';
								$printer .= 	'No se encontraron resultados </em></strong>para la búsqueda <strong><em>"'.$search_term.'"</em></strong>';
								$printer .= '</p>';
							}else{
								$printer .= '<h2 class="title">';
								$printer .= 	'Resultados de búsqueda';
								$printer .= '</h2>';
								$printer .= '<p class="common-box__excerpt">';
								$printer .= 	'Se encontraron <strong><em>'.$resultados.' resultados </em></strong>para la búsqueda <strong><em>"'.$search_term.'"</em></strong>';
								$printer .= '</p>';
							}
								echo $printer;
							?>	
					</div>
				</div>

				<div class="gridle-row">
					<div class="gridle-gr-9 gridle-gr-12@medium">
						<div class="gridle-row">
							<?php
								if(have_posts() && $search_term){
									while(have_posts()){
										the_post();
										$thepost = get_post();
										echo '1';
									}
								}
							?>
							<!--<div class="gridle-gr-4 gridle-gr-12@medium">
								<article>
									<figure class="common-box__figure">
										<a href="#" title="titulo">
											<img src="http://placehold.it/303x300">
										</a>
									</figure>
									<div class="common-box__body">
										<h2 class="main-title--tiny">
											<a href="#" title="titulo" >Tadao Azul</a>
										</h2>

										<p class="common-box__excerpt">
											Lorem ipsum dolor sit amet, onsectetur adipiscing elit.
										</p>

										<p class="common-box__extra">
											<span class="common-box__tag">$42.900</span>
											<a href="#" title="titulo" class="button button--black__small button--small button--ghost float-right" >
												<span>Ver detalles</span>
												<span><i class="icon-elem icon-elem--chevron_right font-color-black" ></i></span>
											</a>
										</p>
									</div>
								</article>
							</div>-->
						</div>
					</div>

					<div class="gridle-gr-3 gridle-gr-12@medium">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php get_footer(); ?>