<footer class="footer-bar">
	<div class="container gridle-no-gutter">
		<div class="gridle-row">
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<a class="app-brand" href="<?php echo home_url(); ?>">
					<img src="<?php bloginfo('template_directory') ;?>/images/logos/logo-footer.png" alt="Logo" class="app-brand__logo">
				</a>
			</div>
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<?php echo print_menu('footer'); ?>
			</div>
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<?php echo print_menu('footer_secondary'); ?>
			</div>
			<div class="gridle-gr-3 gridle-gr-12@medium">
				<p>Centro Comercial Apumanque, local 474, Nivel Manquehue, calle A, Las Condes (Metro Manquehue)</p>
				<p><a href="#" title="titulo">contacto@hilosdeconciencia.cl</a></p>
				<p>
					<?php echo print_menu('footer-rrss'); ?>
				</p>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<?php echo incrustar_scripts(); ?>
</body>

</html>
