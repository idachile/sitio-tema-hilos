<?php
the_post();
get_header();
?>
	<main>
		<section class="horizon horizon__slider bg-white ">
			<div class="container container--border-bot-lighter gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<?php echo getThumbSlider() ?>
					</div>
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<div class="common-box__body common-box__body--no-border">
							<h2 class="main-title"><?php the_title(); ?></h2>
							<p class="common-box__meta">
								<?php
									$on_off=get_field('habilitar_sku');
									if($on_off):
										echo getSku();
									endif ?>
								<em class="secondary-color">Marca</em></p>
							<p class="common-box__meta">
							<p class="common-box__excerpt"><?php echo get_the_excerpt(); ?></p>
							<div class="common-box__extra">
								<p class="font-size-tiny common-box__extra--font grey-color_dark">STOCK: <strong><?php echo get_field('disponibilidad_producto'); ?></strong></p>
								<?php
									$habilitar_talla = get_field('habilitar_talla');
									if($habilitar_talla): ?>
										<div class="gridle-row">
											<div class="gridle-gr-4 gridle-gr-12@medium">
												<p class="font-size-tiny common-box__extra--font grey-color_dark">
													<strong>Variedad de tamaño:</strong>
												</p>
											</div>
											<div class="gridle-gr-8 gridle-gr-12@medium grey-color_dark tallas">
												<?php echo getTallas()?>
											</div>
										</div>
								<?php endif ?>
								<?php
									$habilitar_variedad = get_field('habilitar_variedad');

									if($habilitar_variedad): ?>
										<div class="gridle-row">
											<div class="gridle-gr-4 gridle-gr-12@medium">
												<p class="font-size-tiny common-box__extra--font grey-color_dark">
										 			<strong>Variedad disponible: </strong>
												</p>
											</div>
											<div class="gridle-gr-5 gridle-gr-12@medium fondo-flecha">
												<?php echo getVariedades(); ?>
											</div>
										</div>
								<?php endif ?>
							</div>
							<div class="common-box__extra common-box__extra--font">
								<span class="common-box__tag common-box__tag--highlighted">
									<?php
									$habilitar_precio = get_field('habilitar_precio');
									if($habilitar_precio):
										echo getPrecio();
									endif?>
								</span>
								<a href="#" title="titulo" class="button button--medium button--ghost button--fixed" id="button-cotizar" data-module="modal"
									data-role="modal-btn" data-target="<?php echo get_template_directory_uri() . '/partials/ajax/ajax-template-form-producto.html'; ?>">Cotizar</a>
							</div>
							<div class="common-box__footer common-box__footer--margin-top bg-grey-lightest common-box__body--flex-none">
								Comparte con alguien nuestro producto:
								<a href="#" title="titulo" class="social-link social-link--facebook social-link--ghost social-link--main-bg"></a>
								<a href="#" title="titulo" class="social-link social-link--twitter social-link--ghost social-link--main-bg"></a>
								<a href="#" title="titulo" class="social-link social-link--instagram social-link--ghost social-link--main-bg"></a>
								<a href="#" title="titulo" class="social-link social-link--linkedin social-link--ghost social-link--main-bg"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<?php
							$habilitar_caracteristicas_generales = get_field('habilitar_caracteristicas_generales') ;
							if($habilitar_caracteristicas_generales): ?>
								<div class="common-box__body">
									<h3 class="font-size-medium">Características Generales</h3>
									<p> <?php $caract_generales = get_field('caracteristicas_generales'); echo $caract_generales; ?></p>
								</div>
						<?php endif ?>
					</div>
					<div class="gridle-gr-6 gridle-gr-12@medium">
						<?php
							$habilitar_ficha = get_field('habilitar_ficha_tecnica');
							$caracteristica = get_field('caracteristica');

							if($habilitar_ficha): ?>
								<div class="common-box__body">
									<h3 class="font-size-medium">Ficha Técnica</h3>
									<?php echo getFichaTecnica(); ?>
								</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</section>
		<?php
		$args = array('post_type' => 'producto', 'posts_per_page' => 4);
		echo getProductosDestacados($args,3); ?>

	</main>

	<?php get_footer(); ?>
