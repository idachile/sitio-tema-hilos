<?php
/*Template Name: Catalogo*/
get_header();
?>

	<main>
    <?php
			if($post->post_name == 'hombres' || $post->post_name == 'mujeres' || $post->post_name == 'ninos'):
				$pagename = 'vestimentas/'.$post->post_name;
			else :
				$pagename = $post->post_name;
			endif;

			$args = array('post_type' => 'page','post_status' => 'publish','posts_per_page' => 1,'pagename' => $pagename);

    	echo getDapartamentos($args)

		?>
		<section class="horizon bg-white">
			<div class="container gridle-no-gutter">
				<div class="gridle-row">
					<div class="gridle-gr-3 gridle-gr-12@medium"><?php echo getFiltroProducto()?></div>
					<div class="gridle-gr-9 gridle-gr-12@medium gridle-no-gutter">
						<div class="gridle-row">
							<?php $args = array('post_type' => 'producto','posts_per_page' => -1);

								if ($post->post_name==$post->post_name) :
									$args['tax_query'] = array(
										'relation' => 'OR',
										array(
											'taxonomy' => 'categoria_producto',
											'field'    => 'slug',
											'terms'    =>  $post->post_name
											),
										array(
											'taxonomy' => 'departamento',
											'field'    => 'slug',
											'terms'    =>  $post->post_name
										));
								endif;
							echo getListadoProductos($args);
							?>
						</div>
					</div>
				</div>
		</section>
	</main>
<?php get_footer(); ?>
