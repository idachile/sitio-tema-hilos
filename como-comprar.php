<?php
/*Template name: ¿Cómo comprar?*/
get_header();
?>

<main>
	<section class="horizon horizon_inner horizon--extra bg-white">
		<div class="container">
			<div class="gridle-row">
				<div class="gridle-gr-9 gridle-gr-12@medium">
					<div class="horizon notice-highlighted">
						<?php
							$args = array('post_type' => 'post', 'posts_per_page' => 1);
							$destacada_array = get_field('noticia_destacada');

							if (!empty($destacada_array)):
								$args['post__in'] = $destacada_array;
							endif;

							echo getNoticiaDestacada($args);
		        ?>
					</div>
					<div class="horizon video-container">
						<?php
							$video_bool = get_field('habilitar_video');
							if($video_bool) :
								echo getVideo();
							endif
						?>
					</div>
					<div class="horizon bor-lef-rig galeria">
						<?php echo getThumbSlider()?>
					</div>
					<div class="content-box share bor-lef-rig">
						<p>Comparte este artículo: </p>
						<a href="#" class="social-link social-link--round social-link--facebook">Facebook</a>
						<a href="#" class="social-link social-link--round social-link--twitter">Twitter</a>
					</div>
					<div class="horizon comentarios">
						<?php comments_template(); ?>
					</div>
				</div>
			</div>
				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php
						$on_off = get_field('habilitar_productos_destacados');
						if ($on_off):
							$args = array('post_type'=>'producto','posts_per_page'=>4);
							$destacados_array = get_field('productos_destacados');

							if (!empty($destacados_array)):
								$args['post__in'] = $destacados_array;
							endif;

						echo getProductosDestacados($args,12);
						endif
					?>
				</div>
		</div>
	</section>


	<section class="horizon horizon__inner">
		<div class="container">
			<div class="gridle-row">
				<?php
					$on_off = get_field('habilitar_noticias');
					if($on_off):
						$args = array('post_type' => 'post','posts_per_page' => -1);
						$noticias_array = get_field('noticias');

						if (!empty($noticias_array)):
							$args['post__in'] = $noticias_array;
						endif;

						echo getNoticias($args,9);
						endif
						?>
				<div class="gridle-gr-3 gridle-gr-12@medium">
					<?php
						$sidebar = get_field('tipo_sidebar');
						if($sidebar):
							echo getSidebar();
						endif;
					?>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_footer(); ?>
