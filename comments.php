<?php



    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) {
        die('Please do not load this page directly. Thanks!');
    }
    if (!empty($post->post_password)) {
        if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {
            ?>

<div class="mensaje">
	<p class="mensaje-exclama">Este Post está protegido con password. Ingresa el password para ver los comentarios</p>
</div>

<?php return;
        }
    } ?>

<!------------------------------------------------------------------------------Formulario-->
<?php if ('open' == $post->comment_status) { ?>

  <div class="gridle-gr-12 gridle-gr-12@medium">
    <h2 class="main-title">  Deje su comentario</h2>
  </div>
<div class="gridle-gr-9 gridle-gr-12@medium">
  <div id="respond">
        <form id="commentform" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
          <div class="gridle-row">

            <div class="gridle-gr-6 gridle-gr-12@medium">
							<div class="form-control" data-error-message="Debe ingresar su nombre">
								<label class="form-control__label">Nombre *</label>
								<input name="nombre_comentario" type="text" class="form-control__field" required placeholder="Ingrese su nombre" data-custom-validation="onlyString">
							</div>
						</div>

            <div class="gridle-gr-6 gridle-gr-12@medium">
							<div class="form-control" data-error-message="Debe ingresar un email válido">
								<label class="form-control__label">Correo *(no será publicado)</label>
								<input name="mensaje_comentario" type="email" class="form-control__field" required placeholder="Ingrese su email">
							</div>
						</div>

            <div class="gridle-gr-12 gridle-gr-12@medium">
              <?php do_action('comment_form_top'); ?>
              <div class="form.control">
                <label class="form-control__label">Comentario *</label>
                <textarea id="comment" name="comment" class="form-control__field" placeholder="Deja tu comentario aqui..." rows="5"></textarea>
                <span class="form-control__label font-size-tiny font-color-grey-light">Máx <span id="countpar">500</span> caracteres</span>
              </div>
              <p class="common-box__meta">* Todos los campos son obligatorios</p>
            </div>
          </div>


            <input type="hidden" name="anadirComentario" value="true" />

            <?php comment_id_fields($post->ID); ?>

            <div class="form-submit clearfix">
                <button type="submit" id="submit" class="action-ca evt ganalytics button button--send">Comentar</button>
            </div>
            <?php do_action('comment_form', $post->ID); ?>
        </form>
  </div>

<?php
    } ?>

    <?php if ( have_comments() ) : ?>
      <div class="gridle-gr-12 gridle-gr-12@medium comentarios">

          <?php  wp_list_comments('style=ul&callback=customComment&reply_text=reponder');?>

      </div>
    <?php else : // or, if we don't have comments:
        if ( ! comments_open() ) : ?>
            <p class="nocomments"><?php _e( 'Comments are closed.', 'twentyten' ); ?></p>
        <?php endif; // end ! comments_open() ?>
    <?php endif; // end have_comments() ?>
