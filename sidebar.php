<article class="content-box--grey content__sidebar">
    <div class="common-box__body">
        <p class="common-box__excerpt font-size-regular">Te invitamos a navegar dentro de <strong>nuestras categorías</strong></p>
        <ul>
            <!--<li><a href="#" title="titulo" class="font-color-grey-darkest font-size-regular">Categoría 1</a></li>-->
			<?php
				$categorias = get_categories();
				foreach($categorias as $categ){
					if($categ->slug != 'sin-categoria'){
						$out .= '<li>';
						$out .= 	'<a href="/categorias/'.$categ->slug.'" title="titulo" class="font-color-grey-darkest font-size-regular">';
						$out .= 		$categ->name;
						$out .= 	'</a>';
						$out .= '</li>';
					}
				}
				echo $out;
			?>
		</ul>
	</div>
</article>