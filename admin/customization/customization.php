<?php

/**
 * Clase que se encarga de agregar y configurar los Theme Customization para el One Page
 * Se genera una seccion de opciones para cada horizonte
 */
class Customization {
	public static function register( $wp_customize ) {
		//self::register_general_section( $wp_customize );
		self::register_style_section( $wp_customize );
		//self::register_intro_section( $wp_customize );
		//self::register_content_section( $wp_customize );
		self::register_contact_section( $wp_customize );
		//
		// $wp_customize->get_setting( 'contact_direccion' )->transport = 'postMessage';
		// $wp_customize->get_setting( 'contact_telefono' )->transport = 'postMessage';
		// $wp_customize->get_setting( 'contact_telefono_2' )->transport = 'postMessage';
		// $wp_customize->get_setting( 'contact_contact_email' )->transport = 'postMessage';

	}

	private static function register_general_section( $wp_customize ){
		$wp_customize->add_section('general_section', [
			'title' => 'Opciones generales',
			'description' => 'Configuración de opciones generales del sitio',
			'capability' => 'edit_theme_options',
			'priority' => 50
		]);

		// Campos de redes sociales
		$social_networks = ['facebook', 'twitter', 'instagram'];
		foreach( $social_networks as $network ){
			$wp_customize->add_setting('general_'. $network, [
				'default' => '#',
				'capability' => 'edit_theme_options',
				'transport' => 'postMessage',
				'sanitize_callback' => 'sanitize_text_field',
				'sanitize_js_callback' => 'sanitize_text_field'
			]);

			$wp_customize->add_control('general_'. $network .'_control', [
				'section' => 'general_section',
				'settings' => 'general_'. $network,
				'type' => 'text',
				'label' => 'Perfil de '. ucfirst($network),
				'description' => 'Ingrese la URL del perfil de '. ucfirst($network)
			]);
		}
	}

	private static function register_intro_section( $wp_customize ){
		$wp_customize->add_section('intro_section', [
			'title' => 'Destacados',
			'description' => 'Configuración del slider principal con contenidos destacados',
			'capability' => 'edit_theme_options',
			'priority' => 51
		]);

		// Campo para la seleccion de post para esta seccion
		$wp_customize->add_setting('intro_posts', [
			'capability' => 'edit_theme_options'
		]);

		require_once 'controls/post-selector-control.php';
		$wp_customize->add_control(new Post_Selector_Custom_Control(
			$wp_customize,
			'intro_posts_control',
			[
				'label' => 'Destacados',
				'description' => 'Seleccione uno o más ítems. Puedes seleccionar mas de uno manteniendo presionada la tecla "ctrl"',
				'section' => 'intro_section',
				'settings' => 'intro_posts'
			],
			[
				'post_type' => 'slide'
			]
		));
	}

	private static function register_content_section( $wp_customize ){
		$wp_customize->add_section('content_section', [
			'title' => 'Contenido',
			'description' => 'Configuración de las cajas de contenido',
			'capability' => 'edit_theme_options',
			'priority' => 52
		]);

		// Campo para el titulo de la seccion
		$wp_customize->add_setting('content_title', [
			'default' => 'Quiénes Somos',
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('content_title_control', [
			'section' => 'content_section',
			'settings' => 'content_title',
			'type' => 'text',
			'label' => 'Título de la sección',
			'description' => 'Ingrese el título de la sección'
		]);

		// Campo para la seleccion de los posts de contenido
		$wp_customize->add_setting('content_posts', [
			'capability' => 'edit_theme_options'
		]);

		require_once 'controls/post-selector-control.php';
		$wp_customize->add_control(new Post_Selector_Custom_Control(
			$wp_customize,
			'content_posts_control',
			[
				'label' => 'Servicios',
				'description' => 'Seleccione uno o más ítems. Puedes seleccionar mas de uno manteniendo presionada la tecla "ctrl"',
				'section' => 'content_section',
				'settings' => 'content_posts'
			],
			[
				'post_type' => 'servicio'
			]
		));
	}

	private static function register_contact_section( $wp_customize ){
		$wp_customize->add_section('contact_section', [
			'title' => 'Contacto',
			'description' => 'Configuración de la sección de contacto',
			'capability' => 'edit_theme_options',
			'priority' => 53
		]);


		// Campo para el email de contacto
		$wp_customize->add_setting('contact_contact_email', [
			'default' => get_bloginfo('admin_email'),
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('email_footer_control', [
			'section' => 'contact_section',
			'settings' => 'contact_contact_email',
			'type' => 'email',
			'label' => 'Email Footer',
			'description' => 'Ingrese que desea poner en el formulario'
		]);


			// Campo para el cc de contacto
			$wp_customize->add_setting('contact_to_email', [
				'default' => get_bloginfo('admin_email'),
				'capability' => 'edit_theme_options',
				'transport' => 'postMessage',
				'sanitize_callback' => 'sanitize_text_field',
				'sanitize_js_callback' => 'sanitize_text_field'
			]);

			$wp_customize->add_control('email_to_control', [
				'section' => 'contact_section',
				'settings' => 'contact_to_email',
				'type' => 'email',
				'label' => 'Email Principal Formulario',
				'description' => 'Ingrese el email principal al cual desea dirigir el formulario de contacto'
			]);

		// Campo para el cc de contacto
		$wp_customize->add_setting('contact_cc_email', [
			'default' => get_bloginfo('admin_email'),
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('email_forms_control', [
			'section' => 'contact_section',
			'settings' => 'contact_cc_email',
			'type' => 'email',
			'label' => 'Email Secundario Formulario',
			'description' => 'Ingrese el email secundario al cual desea dirigir el formulario de contacto'
		]);




		// campo para la direccion fisica de la empresa
		$wp_customize->add_setting('contact_direccion', [
			'default' => '#',
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('contact_direccion_control', [
			'section' => 'contact_section',
			'settings' => 'contact_direccion',
			'type' => 'text',
			'label' => 'Dirección',
			'description' => 'Ingrese la dirección física de la empresa'
		]);

		// campo para el horario de atención
		$wp_customize->add_setting('contact_horario', [
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('contact_horario_control', [
			'section' => 'contact_section',
			'settings' => 'contact_horario',
			'type' => 'text',
			'label' => 'Horario de atención',
			'description' => 'Ingrese el horario de atención'
		]);

		// campo para el telefono 1
		$wp_customize->add_setting('contact_telefono', [
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('contact_telefono_control', [
			'section' => 'contact_section',
			'settings' => 'contact_telefono',
			'type' => 'text',
			'label' => 'Teléfono de contacto',
			'description' => 'Ingrese el teléfono de contacto'
		]);
		// campo para el telefono 2
		$wp_customize->add_setting('contact_telefono_2', [
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('contact_telefono_control_2', [
			'section' => 'contact_section',
			'settings' => 'contact_telefono_2',
			'type' => 'text',
			'label' => 'Teléfono de contacto seundario (opcional)',
			'description' => 'Ingrese el teléfono de contacto'
		]);
	}

	private static function register_style_section( $wp_customize ){
		$wp_customize->add_section('style_section', [
			'title' => 'Child Theme',
			'description' => 'Se debe seleccionar un child theme para el sitio actual',
			'capability' => 'edit_theme_options',
			'priority' => 54
		]);

		// Campo para el titulo de la seccion
		$wp_customize->add_setting('style_css_control', [
			'default' => '',
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
			'sanitize_js_callback' => 'sanitize_text_field'
		]);

		$wp_customize->add_control('style_css_control', [
			'section' => 'style_section',
			'settings' => 'style_css_control',
			'type' => 'select',
			'choices'  => array(
						'' =>'Elije una opción',
						get_bloginfo('template_directory') . '/css/'.'main-cimon.css'  => 'cimon',
						get_bloginfo('template_directory') . '/css/'.'main-ovisnova.css'  => 'ovisnova',
						get_bloginfo('template_directory') . '/css/'.'main-bahia-lomas.css'  => 'Bahia Lomas',
						get_bloginfo('template_directory') . '/css/'.'main-austral-biotech.css'  => 'Austral Biotech',
						get_bloginfo('template_directory') . '/css/'.'main-capia.css'  => 'capia',
						get_bloginfo('template_directory') . '/css/'.'main-cieduca.css'  => 'cieduca',
						get_bloginfo('template_directory') . '/css/'.'main-ciicc.css'  => 'ciicc ',
						get_bloginfo('template_directory') . '/css/'.'main-tekit.css'  => 'tekit ',
						get_bloginfo('template_directory') . '/css/'.'main-cielo.css'  => 'cielo ',
						get_bloginfo('template_directory') . '/css/'.'main-cigap.css'  => 'cigap '
					),
			'label' => 'Selecciona un theme',
			'description' => 'Cambiará la paleta cromática'
		]);

		$wp_customize->remove_control('background_image');
		$wp_customize->remove_control('link_color');
		$wp_customize->remove_section('colors');

		// Campo para el titulo de la seccion
		$wp_customize->add_setting('style_logo', [
			'default' => '',
			'capability' => 'edit_theme_options',
			'transport' => 'postMessage'
		]);
		$wp_customize->add_control(
		       new WP_Customize_Image_Control(
		           $wp_customize,
		           'style_logo',
		           array(
		               'label'      => 'Logo Footer',
		               'section'    => 'style_section',
		               'settings'   => 'style_logo'
		           )
		       )
		   );

	}
}


add_action( 'customize_register' , array( 'Customization' , 'register' ) );
