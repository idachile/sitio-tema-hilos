<?php
if ( ! class_exists( 'WP_Customize_Control' ) ) {
	return NULL;
}


class Post_Selector_Custom_Control extends WP_Customize_Control {
	public function __construct( $manager, $id, $args = [], $options = [] ){
		$this->options = wp_parse_args( $options, [
			'post_type' => 'post',
			'multiple' => 'multiple'
		]);

		parent::__construct( $manager, $id, $args );
	}

	public function render_content(){
		$query = new WP_Query([
			'post_type' => $this->options['post_type'],
			'posts_per_page' => 200,
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'DESC'
		]);

		$value = $this->value();
		if( empty($value) ){ $value = []; }
		?>
			<label>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
				<select <?php $this->link(); ?> <?php echo $this->options['multiple']; ?> name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>" style="display: block; width: 100%;" size="10">
				<?php
					if( $query->have_posts() ){
						while( $query->have_posts() ){
							$query->the_post();
							$id = get_the_ID();					
							$selected = in_array( $id, $value ) ? 'selected="selected"' : '';

							echo '<option '. $selected .' value="'. $id .'" >'. get_the_title() .'</option>';
						}
						wp_reset_query();
					}
					else {
						echo '<option value="" >No se encontraron entradas</option>';
					}
				?>
				</select>
			</label>
		<?php
	}
}