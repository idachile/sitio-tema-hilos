<?php

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5' );
add_theme_support( 'menus' );
add_post_type_support( 'page', 'excerpt' );
/**
 * Quita los menus por defecto
 * 	- Entradas
 * 	- Páginas
 * 	- Sugerencias
 * 	- Comentarios
 */
add_action( 'admin_menu', 'renex_remove_menus' );
function renex_remove_menus(){
	//remove_menu_page( 'edit.php' );
	//remove_menu_page( 'edit.php?post_type=page' );
	remove_menu_page( 'edit.php?post_type=feedback' );
	remove_menu_page( 'edit-comments.php' );
}

/**
 * Quita los menus por defecto de la barra superior
 * 	- Nuevo Post
 * 	- Nueva Página
 * 	- Nuevo Link
 */
add_action( 'admin_bar_menu', 'renex_remove_top_nodes', 999 );
function renex_remove_top_nodes(){
	global $wp_admin_bar;

		// $wp_admin_bar->remove_node( 'new-post' );
    // $wp_admin_bar->remove_node( 'new-page' );
    $wp_admin_bar->remove_node( 'new-link' );
}
