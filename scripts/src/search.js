$(function(){
  var $searchbar  = $('#searchbar');  
  $('#searchtoggl').on('click', function(e){
    e.preventDefault();  
      $searchbar.slideToggle(300, function(){
        // callback after search bar animation
      }); 
  });  
  $('#searchform').submit(function(e){
    e.preventDefault(); // stop form submission
  });
});