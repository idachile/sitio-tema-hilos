(function(window, document, $){
	"use strict";

	var DEBUG = true;

	var $window = $(window),
		$document = $(document),
		$body = $('body'),
		$mainNav, $mainHeader;

	/// guardo los media queries
	var TABLETS_DOWN = 'screen and (max-width: 64em)',
		VERTICAL_TABLETS_DOWN = 'screen and (max-width: 50em)',
		VERTICAL_TABLETS_UP = 'screen and (min-width: 50em)',
		PHABLETS_DOWN = 'screen and (max-width: 40em)';

	var throttle = function( fn ){
		return setTimeout(fn, 1);
	};

	var mqMap = function( mq ){
		var MQ = '';

		switch( mq ){
			case 'tablet-down' :
				MQ = TABLETS_DOWN;
				break;
			case 'vertical-tablet-down' :
				MQ = VERTICAL_TABLETS_DOWN;
				break;
			case 'phablet-down' :
				MQ = PHABLETS_DOWN;
				break;
		}

		return MQ;
	};

	var normalize = (function() {
			var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
				to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
				mapping = {};

			for(var i = 0, j = from.length; i < j; i++ )
				mapping[ from.charAt( i ) ] = to.charAt( i );

			return function( str ) {
				var ret = [];
				for( var i = 0, j = str.length; i < j; i++ ) {
					var c = str.charAt( i );
					if( mapping.hasOwnProperty( str.charAt( i ) ) )
						ret.push( mapping[ c ] );
					else
						ret.push( c );
				}
				return ret.join( '' );
			};
		})();


	// trackeos de analytics
	var track = function( data ){
		if( !dataLayer ){
			DEBUG && console.log('No hay dataLayer');
			return;
		}

		dataLayer.push(data);
		DEBUG && console.log('Enviado a GTM', data);
	};


	var App = function(){};

	App.prototype = {
		onReady : function(){
			this.conditionalInits();

			// // activa trackeos de eventos en GTM
			// $body.on('click.st_gtm', '[data-track]', function(){
			// 	var $item = $(this);

			// 	track({
			// 		event : $item.data('gtm-event'),
			// 		eventCategory : $item.data('gtm-eventcategory'),
			// 		eventAction : $item.data('gtm-eventaction'),
			// 		eventLabel : $item.data('gtm-eventlabel')
			// 	});
			// });
		},
		
		onLoad : function(){
			// // videos elasticos
			// $('[data-multimedia]').fitVids();

			// throttle(function(){
            //     $('[data-equalize="children"][data-mq="tablet-down"]').equalizeChildrenHeights(true, TABLETS_DOWN);
            //     $('[data-equalize="children"][data-mq="vertical-tablet-down"]').equalizeChildrenHeights(true, VERTICAL_TABLETS_DOWN);
            //     $('[data-equalize="target"][data-mq="vertical-tablet-down"]').equalizeTarget(true, VERTICAL_TABLETS_DOWN);
            //     $('[data-equalize="target"][data-mq="phablet-down"]').equalizeTarget(true, PHABLETS_DOWN);

            //     $('[data-equalize="children"][data-mq="all"]').equalizeChildrenHeights(true);
            // });
		},
		
		autoHandleEvents : function( $elements ){
			if( !$elements || !$elements.length ){ return false; }

			var self = this;

			$elements.each(function(i,el){
				var func = el.getAttribute('data-func') || false,
					evts = el.getAttribute('data-events') || 'click.customStuff';

				if( func && typeof( self[func] ) === 'function' ){
					$(el)
						.off(evts)
						.on(evts, $.proxy(self[func], self))
						.attr('data-delegated', 'true');
				}
			});
		},

		conditionalInits : function( $context ){
			if( !$context ){
				$context = $document;
			}

			// delegaciones directas
			if( $context.find('[data-func]').length ){
				this.autoHandleEvents( $context.find('[data-func]') );
			}

			// Formularios genericos
			if( $context.find('[data-validate]').length ){
				$context.find('[data-validate]').validizr({
					validInputCallback : function( $input ){
						$input.parents('.field').removeClass('invalid-input');
					},
					notValidInputCallback : function( $input ){
						$input.parents('.field').addClass('invalid-input');
					}
				});
				
				$('#mensajeform').on('keyup', function( event ){
					var $textarea = $(this),
						maxlength = parseInt($textarea.attr('maxlength')),
						valuelength = $textarea.val().length;

					$('#countpar').text( maxlength - valuelength );
				});



				// contact form
				// var contactArea = function(){
				// 	$arealength = document.getElementById("contacto-msg").length;
					
				// };
			}

			//main slider
			if( $context.find('[data-module="main-slider"]').length ){
				this.mainSlider( $context.find('[data-module="main-slider"]') );
			}

			//single slider
			if( $context.find('[data-module="single-gallery"]').length ){
				this.singleSlider( $context.find('[data-module="single-gallery"]') );
			}

			//complex slider
			if( $context.find('[data-module="complex-gallery"]').length ){
				this.complexSlider( $context.find('[data-module="complex-gallery"]') );
			}
		},

		////////////
		//////////// Delegaciones directas
		////////////
		calendarControl : function( event ){
			event.preventDefault();

			var app = this;

			var $btn = $(event.currentTarget),
				$dataHolder = $btn.parents('[data-role="calendar-data"]'),
				$itemsHolder = $('[data-events-holder]'),
				$monthName = $('[data-role="calendar-month"]'),
				direction = $btn.data('direction'),
				month = $dataHolder.data('month'),
				year = $dataHolder.data('year'),
				filter = $dataHolder.data('filter');

			if( app.isLoading ){
				return;
			}

			app.isLoading = true;

			/// se debe indicar que se esta cargando, asumo estados estandar
            /// eso queire decir, opacity nomas
            $monthName.css('opacity', '0.2');
            $itemsHolder.css('opacity', '0.2');

			$.getJSON('/wp-json/st-rest/actividades', {
				direction : direction,
				month : month,
				year : year,
				filter : filter
			}).then(function( response ){
				$dataHolder.data('month', response.month_num);
                $dataHolder.data('year', response.year);

                $monthName
                    .text( response.month_name + ' - ' +  response.year)
                    .attr({
                        'data-prev' : response.prev,
                        'data-next' : response.next
                    });

                $itemsHolder.html( response.items );

                $monthName.css('opacity', '1');
                $itemsHolder.css('opacity', '1');

                app.isLoading = false;
			});
		},

		deployTarget : function( event ){
			event.preventDefault();

			var $item = $(event.currentTarget),
				target = $item.data('target'),
				$targetElem;

			if( !target ){
				console.warn('No se especificó un objetivo en el atributo "data-target":', target );
				return;
			}

			$targetElem = $(target);
			if( !$targetElem.length ){
				console.warn('El objetivo no fue encontrado o el atributo "data-target" no es un selector válido :', target );
				return;
			}

			$item.data('animating', true);

			if( $item.data('deployed') ){
				$targetElem
					.slideUp().promise()
					.then(function(){
						$item.data({
							deployed : false,
							animating : false
						}).removeClass('deployed');

						$targetElem.removeClass('deployed');
					});
			}
			else {
				$targetElem
					.slideDown().promise()
					.then(function(){
						$item.data({
							deployed : true,
							animating : false
						}).addClass('deployed');

						$targetElem.addClass('deployed');
					});
			}
		},

		deployMainNav : function( event ){
			event.preventDefault();

			var $btn = $(event.currentTarget),
				$nav = $('[data-role="nav-container"]'),
				$headerBody = $('[data-role="header-body"]');

			if( $btn.data('deployed') ) {
				$btn
					.data('deployed', false)
					.removeClass('deployed');

				$nav.removeClass('deployed');
				$headerBody.removeClass('deployed');
			}
			else {
				$btn
					.data('deployed', true)
					.addClass('deployed');

				$nav.addClass('deployed');
				$headerBody.addClass('deployed');
			}
		},

		deployCollapsable : function( event ) {
			event.preventDefault();

			event.preventDefault();

			var $item = $(event.currentTarget),
				$targetElem = $item.parents('.collapsable').find('.collapsable-body');

			$item.data('animating', true);

			if( $item.data('deployed') ){
				$targetElem
					.slideUp().promise()
					.then(function(){
						$item.data({
							deployed : false,
							animating : false
						}).removeClass('deployed');

						$targetElem.removeClass('deployed');
					});
			}
			else {
				$targetElem
					.slideDown().promise()
					.then(function(){
						$item.data({
							deployed : true,
							animating : false
						}).addClass('deployed');

						$targetElem.addClass('deployed');
					});
			}
		},

		closeModal : function( event ){
			event.preventDefault();
			$('[data-module="modal"]').remove();
		}
	};

	var app = new App();

    $document.ready(function(){ app.onReady && app.onReady(); });

    $window.on({
        'load' : function(){ app.onLoad && app.onLoad(); }
    });
	
}(window, document, jQuery));
